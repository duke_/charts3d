//
//  UIView+Autolayout.swift
//
//  Created by Duke Browning on 7/22/17.
//  Copyright (c) 2017 Atomic Skeeter Software. All rights reserved.
//

import UIKit

extension UIView {
    
    @objc @discardableResult func constrainLeading(to otherView : UIView, withOffset offset: CGFloat = 0, isMinimum : Bool = false, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        var constraint : NSLayoutConstraint
        if isMinimum {
            constraint = self.leadingAnchor.constraint(greaterThanOrEqualTo: otherView.leadingAnchor, constant: offset)
        }
        else {
            constraint = self.leadingAnchor.constraint(equalTo: otherView.leadingAnchor, constant: offset)
        }
        constraint.priority = priority
        constraint.isActive = isActive
        return constraint
    }
    
    @objc @discardableResult func constrainLeadingToSuperview(withInset inset : CGFloat = 0, isMinimum : Bool = false, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        assert(superview != nil, "no superview")
        return constrainLeading(to: superview!, withOffset: inset, isMinimum: isMinimum, isActive: isActive, priority: priority)
    }
    
    @objc @discardableResult func constrainTrailing(to otherView : UIView, withOffset offset: CGFloat = 0, isMinimum : Bool = false, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required ) -> NSLayoutConstraint {
        var constraint : NSLayoutConstraint
        if isMinimum {
            constraint = self.trailingAnchor.constraint(lessThanOrEqualTo: otherView.trailingAnchor, constant: offset)
        }
        else {
            constraint = self.trailingAnchor.constraint(equalTo: otherView.trailingAnchor, constant: offset)
        }
        constraint.priority = priority
        constraint.isActive = isActive
        return constraint
    }
    
    @objc @discardableResult func constrainTrailingToSuperview(withInset inset : CGFloat = 0, isMinimum : Bool = false, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        assert(superview != nil, "no superview")
        return constrainTrailing(to: superview!, withOffset: -inset, isMinimum: isMinimum, isActive: isActive, priority: priority)
    }
    
    @objc @discardableResult func constrainTop(to otherView : UIView, withOffset offset: CGFloat = 0, isMinimum : Bool = false, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        var constraint : NSLayoutConstraint
        if isMinimum {
            constraint = self.topAnchor.constraint(greaterThanOrEqualTo: otherView.topAnchor, constant: offset)
        }
        else {
            constraint = self.topAnchor.constraint(equalTo: otherView.topAnchor, constant: offset)
        }
        constraint.priority = priority
        constraint.isActive = isActive
        return constraint
    }
    
    @objc @discardableResult func constrainTopToSuperview(withInset inset : CGFloat = 0, isMinimum : Bool = false, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        assert(superview != nil, "no superview")
        return constrainTop(to: superview!, withOffset: inset, isMinimum: isMinimum, isActive: isActive, priority: priority)
    }
    
    @objc @discardableResult func constrainTopToSafeAreaOfSuperview(withInset inset : CGFloat = 0, isMinimum : Bool = false, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        assert(superview != nil, "no superview")
        if #available(iOS 11.0, *) {
            return constrainTopToSafeArea(of: superview!, withOffset: inset, isMinimum: isMinimum, isActive: isActive, priority: priority)
        } else {
            return constrainTop(to: superview!, withOffset: inset, isMinimum: isMinimum, isActive: isActive, priority: priority)
        }
    }
    
    @objc @discardableResult func constrainTopToSafeArea(of otherView : UIView, withOffset offset: CGFloat = 0, isMinimum : Bool = false, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        var constraint : NSLayoutConstraint
        if isMinimum {
            if #available(iOS 11.0, *) {
                constraint = self.topAnchor.constraint(greaterThanOrEqualTo: otherView.safeAreaLayoutGuide.topAnchor, constant: offset)
            } else {
                constraint = self.topAnchor.constraint(greaterThanOrEqualTo: otherView.topAnchor, constant: offset)
            }
        }
        else {
            if #available(iOS 11.0, *) {
                constraint = self.topAnchor.constraint(equalTo: otherView.safeAreaLayoutGuide.topAnchor, constant: offset)
            } else {
                constraint = self.topAnchor.constraint(equalTo: otherView.topAnchor, constant: offset)
            }
        }
        constraint.priority = priority
        constraint.isActive = isActive
        return constraint
    }
    
    @objc @discardableResult func constrainBottom(to otherView : UIView, withOffset offset: CGFloat = 0, isMinimum : Bool = false, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        var constraint : NSLayoutConstraint
        if isMinimum {
            constraint = self.bottomAnchor.constraint(lessThanOrEqualTo: otherView.bottomAnchor, constant: offset)
        }
        else {
            constraint = self.bottomAnchor.constraint(equalTo: otherView.bottomAnchor, constant: offset)
        }
        constraint.priority = priority
        constraint.isActive = isActive
        return constraint
    }
    
    @objc @discardableResult func constrainBottomToSuperview(withInset inset : CGFloat = 0, isMinimum : Bool = false, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        assert(superview != nil, "no superview")
        return constrainBottom(to: superview!, withOffset: -inset, isMinimum: isMinimum, isActive: isActive, priority: priority)
    }
    
    @objc @discardableResult func constrainBottomToSafeAreaOfSuperview(withInset inset : CGFloat = 0, isMinimum : Bool = false, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        assert(superview != nil, "no superview")
        return constrainBottomToSafeArea(of: superview!, withOffset: -inset, isMinimum: isMinimum, isActive: isActive, priority: priority)
    }
    
    @objc @discardableResult func constrainBottomToSafeArea(of otherView : UIView, withOffset offset: CGFloat = 0, isMinimum : Bool = false, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        var constraint : NSLayoutConstraint
        if isMinimum {
            if #available(iOS 11.0, *) {
                constraint = self.bottomAnchor.constraint(greaterThanOrEqualTo: otherView.safeAreaLayoutGuide.bottomAnchor, constant: offset)
            } else {
                constraint = self.bottomAnchor.constraint(greaterThanOrEqualTo: otherView.bottomAnchor, constant: offset)
            }
        }
        else {
            if #available(iOS 11.0, *) {
                constraint = self.bottomAnchor.constraint(equalTo: otherView.safeAreaLayoutGuide.bottomAnchor, constant: offset)
            } else {
                constraint = self.bottomAnchor.constraint(equalTo: otherView.bottomAnchor, constant: offset)
            }
        }
        constraint.priority = priority
        constraint.isActive = isActive
        return constraint
    }
    
    @objc @discardableResult func constrainAsTrailing(to otherView : UIView, withOffset offset: CGFloat = 0, isMinimum : Bool = false, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required ) -> NSLayoutConstraint {
        var constraint : NSLayoutConstraint
        if isMinimum {
            constraint = self.leadingAnchor.constraint(greaterThanOrEqualTo: otherView.trailingAnchor, constant: offset)
        }
        else {
            constraint = self.leadingAnchor.constraint(equalTo: otherView.trailingAnchor, constant: offset)
        }
        constraint.priority = priority
        constraint.isActive = isActive
        return constraint
    }
    
    @objc @discardableResult func constrainAsLeading(to otherView : UIView, withOffset offset: CGFloat = 0, isMinimum : Bool = false, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required ) -> NSLayoutConstraint {
        return otherView.constrainAsTrailing(to: self, withOffset: offset, isMinimum: isMinimum, isActive: isActive, priority: priority)
    }
    
    @objc @discardableResult func constrainAsBelow(to otherView : UIView, withOffset offset: CGFloat = 0, isMinimum : Bool = false, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required ) -> NSLayoutConstraint {
        var constraint : NSLayoutConstraint
        if isMinimum {
            constraint = self.topAnchor.constraint(greaterThanOrEqualTo: otherView.bottomAnchor, constant: offset)
        }
        else {
            constraint = self.topAnchor.constraint(equalTo: otherView.bottomAnchor, constant: offset)
        }
        constraint.priority = priority
        constraint.isActive = isActive
        return constraint
    }
    
    @objc @discardableResult func constrainAsAbove(to otherView : UIView, withOffset offset: CGFloat = 0, isMinimum : Bool = false, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required ) -> NSLayoutConstraint {
        return otherView.constrainAsBelow(to: self, withOffset: offset, isMinimum: isMinimum, isActive: isActive, priority: priority)
    }
    
    @objc @discardableResult func constrainXAligned(to otherView : UIView, withOffset offset: CGFloat = 0, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        let constraint = self.centerXAnchor.constraint(equalTo: otherView.centerXAnchor, constant: offset)
        constraint.priority = priority
        constraint.isActive = isActive
        return constraint
    }
    
    @objc @discardableResult func constrainYAligned(to otherView : UIView, withOffset offset: CGFloat = 0, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        let constraint = self.centerYAnchor.constraint(equalTo: otherView.centerYAnchor, constant: offset)
        constraint.priority = priority
        constraint.isActive = isActive
        return constraint
    }
    
    @objc @discardableResult func constrainWidth(to width : CGFloat, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        let constraint = self.widthAnchor.constraint(equalToConstant: width)
        constraint.priority = priority
        constraint.isActive = isActive
        return constraint
    }
    
    @objc @discardableResult func constrainMinWidth(to width : CGFloat, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        let constraint = self.widthAnchor.constraint(greaterThanOrEqualToConstant: width)
        constraint.priority = priority
        constraint.isActive = isActive
        return constraint
    }
    
    @objc @discardableResult func constrainMaxWidth(to width : CGFloat, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        let constraint = self.widthAnchor.constraint(lessThanOrEqualToConstant: width)
        constraint.priority = priority
        constraint.isActive = isActive
        return constraint
    }
    
    @objc @discardableResult func constrainHeight(to height : CGFloat, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        let constraint = self.heightAnchor.constraint(equalToConstant: height)
        constraint.priority = priority
        constraint.isActive = isActive
        return constraint
    }
    
    @objc @discardableResult func constrainMinHeight(to height : CGFloat, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        let constraint = self.heightAnchor.constraint(greaterThanOrEqualToConstant: height)
        constraint.priority = priority
        constraint.isActive = isActive
        return constraint
    }
    
    @objc @discardableResult func constrainMaxHeight(to height : CGFloat, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        let constraint = self.heightAnchor.constraint(lessThanOrEqualToConstant: height)
        constraint.priority = priority
        constraint.isActive = isActive
        return constraint
    }
    
    @objc @discardableResult func constrainSidesToSuperview(withInset inset: CGFloat = 0, isMinimum : Bool = false, priority : UILayoutPriority = UILayoutPriority.required) -> [NSLayoutConstraint] {
        let leading = constrainLeadingToSuperview(withInset: inset, isMinimum: isMinimum, priority: priority)
        let trailing = constrainTrailingToSuperview(withInset: inset, isMinimum: isMinimum, priority: priority)
        return [leading, trailing]
    }
    
    @objc @discardableResult func constrainTopAndBottomToSuperview(withInset inset: CGFloat = 0, isMinimum : Bool = false, priority : UILayoutPriority = UILayoutPriority.required) -> [NSLayoutConstraint] {
        let top = constrainTopToSuperview(withInset: inset, isMinimum: isMinimum, priority: priority)
        let bottom = constrainBottomToSuperview(withInset: inset, isMinimum: isMinimum, priority: priority)
        return [top, bottom]
    }
    
    @objc @discardableResult func constrainAllSidesToSuperview(withInset inset: CGFloat = 0, isMinimum: Bool = false, priority : UILayoutPriority = UILayoutPriority.required) -> [NSLayoutConstraint] {
        let top = constrainTopToSuperview(withInset: inset, isMinimum: isMinimum, priority: priority)
        let left = constrainLeadingToSuperview(withInset: inset, isMinimum: isMinimum, priority: priority)
        let right = constrainTrailingToSuperview(withInset: inset, isMinimum: isMinimum, priority: priority)
        let bottom = constrainBottomToSuperview(withInset: inset, isMinimum: isMinimum, priority: priority)
        return [top, left, bottom, right]
    }
    
    @objc @discardableResult func constrainAllSides(to view: UIView, withInset inset: CGFloat = 0, isMinimum: Bool = false, priority : UILayoutPriority = UILayoutPriority.required) -> [NSLayoutConstraint] {
        let top = constrainTop(to: view, withOffset: inset, isMinimum: isMinimum, priority: priority)
        let left = constrainLeading(to: view, withOffset: inset, isMinimum: isMinimum, priority: priority)
        let right = constrainTrailing(to: view, withOffset: -inset, isMinimum: isMinimum, priority: priority)
        let bottom = constrainBottom(to: view, withOffset: -inset, isMinimum: isMinimum, priority: priority)
        return [top, left, bottom, right]
    }
    
    @objc @discardableResult func constrainXAlignedToSuperview(withOffset offset: CGFloat = 0, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        assert(superview != nil, "no superview")
        return constrainXAligned(to: superview!, withOffset: offset, isActive: isActive, priority: priority)
    }
    
    @objc @discardableResult func constrainYAlignedToSuperview(withOffset offset: CGFloat = 0, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        assert(superview != nil, "no superview")
        return constrainYAligned(to: superview!, withOffset: offset, isActive: isActive, priority: priority)
    }
    
    func centerInSuperview(hOffset : CGFloat = 0, vOffset : CGFloat = 0) {
        assert(superview != nil, "no superview")
        constrainXAligned(to: superview!, withOffset: hOffset)
        constrainYAligned(to: superview!, withOffset: vOffset)
    }
    
    @objc @discardableResult func constrainHeightToSameAs(view : UIView, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: view, attribute: .height, multiplier: 1.0, constant: 0.0)
        constraint.priority = priority
        constraint.isActive = isActive
        return constraint
    }
    
    @objc @discardableResult func constrainHeightToSameAsWidth(multiplier : CGFloat = 1.0, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: self, attribute: .width, multiplier: multiplier, constant: 0.0)
        constraint.priority = priority
        constraint.isActive = isActive
        return constraint
    }
    
    @objc @discardableResult func constrainWidthToSameAs(view : UIView, isActive : Bool = true, priority : UILayoutPriority = UILayoutPriority.required) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 1.0, constant: 0.0)
        constraint.priority = priority
        constraint.isActive = isActive
        return constraint
    }
    
    @objc @discardableResult func addConstraints(fromFormat format: String, metrics : [String:NSNumber]? = nil, views : [String:UIView]) -> [NSLayoutConstraint] {
        
        let constraints = NSLayoutConstraint.constraints(withVisualFormat: format, metrics: metrics, views: views)
        
        addConstraints(constraints)
        
        for constraint in constraints {
            constraint.isActive = true
        }
        
        return constraints
    }
    
    // Constrains this view's subviews to be a column (or row) of evenly spaced views.
    // By default, the sides (or top/bottom) are constrained to this (superview), but can just be
    // aligned to center using the shouldCenter parameter.
    func constrainSubviews(asRow : Bool = false,
                           spacing : CGFloat = 0,
                           shouldCenter : Bool = false,
                           startMargin : CGFloat = 0,
                           endMargin : CGFloat = 0,
                           noEndConstraint : Bool = false) {
        
        var lastView : UIView?
        if asRow {
            for view in subviews {
                if lastView == nil {
                    view.constrainLeadingToSuperview(withInset: startMargin)
                }
                else {
                    view.constrainAsTrailing(to: lastView!, withOffset: spacing)
                }
                
                if shouldCenter {
                    view.constrainYAlignedToSuperview()
                }
                else {
                    view.constrainTopToSuperview()
                    view.constrainBottomToSuperview()
                }
                
                lastView = view
            }
            if !noEndConstraint { lastView?.constrainTrailingToSuperview(withInset: endMargin) }
        }
        else { // as column
            for view in subviews {
                if lastView == nil {
                    view.constrainTopToSuperview(withInset: startMargin)
                }
                else {
                    view.constrainAsBelow(to: lastView!, withOffset: spacing)
                    view.constrainHeightToSameAs(view: lastView!)
                }
                
                if shouldCenter {
                    view.constrainXAlignedToSuperview()
                }
                else {
                    view.constrainLeadingToSuperview()
                    view.constrainTrailingToSuperview()
                }
                
                lastView = view
            }
            
            if !noEndConstraint { lastView?.constrainBottomToSuperview(withInset: endMargin) }
        }
    }
}

extension UIImageView {
    
    func constrainToImageSize() {
        guard image != nil else {
            print("UIImageView constrainToImageSize does nothing if image is nil.")
            return
        }
        
        constrainWidth(to: image!.size.width)
        constrainHeight(to: image!.size.height)
    }
}
