//
//  AirportDiagram.swift
//  Terrain3D
//
//  Created by Duke Browning on 12/4/18.
//  Copyright © 2018 Atomic Skeeter Software, LLC. All rights reserved.
//

import Foundation

class AirportDiagram : Chart {
    
    let elevation : Double
    var selectedRunwayKey : String?
    var runwayKeys : [String]?

    init(code : String, refImageName : String,
         refPt : CGPoint, refLat : Double, refLng : Double,
         ptPerMinuteLat : Double, ptPerMinuteLng : Double, elevation : Double) {
  
        self.elevation = elevation
        
        super.init(code: code, refImageName: refImageName,
                   refPt : refPt, refLat : refLat, refLng : refLng,
                   ptPerMinuteLat : ptPerMinuteLat, ptPerMinuteLng : ptPerMinuteLng)
    }
}

