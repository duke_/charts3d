//
//  TerrainMesh.swift
//
//  Created by Duke Browning on 11/23/18.
//  Copyright © 2018 Atomic Skeeter Software, LLC. All rights reserved.
//

import Foundation
import SceneKit

typealias HeightProvider = ((Int, Int) -> (Double))

class TerrainMesh: SCNNode {
    private var vertScale = 128 // inverse effect
    private var numRowsNS = 32
    private var numColsNS = 32
    private var meshGeometry: SCNGeometry!
    private var color = UIColor.black
    
    var heightProvider: HeightProvider?
    
    init(rowsNS: Int, colsEW: Int) {
        super.init()
        self.numRowsNS = rowsNS
        self.numColsNS = colsEW
    }
    
    func heightAt(x: Int, y: Int) -> Double {
        return heightProvider?(x, y) ?? 0.0
    }
    
    private func SCNVector2MakeDbl(_ x : Double, _ y : Double) -> vector_float2 {
        return vector_float2(Float(x), Float(y))
    }
    
    private func createGeometry() ->SCNGeometry {
        let cint: CInt = 0
        let sizeOfCInt = MemoryLayout.size(ofValue: cint)
        let float: Float = 0.0
        let sizeOfFloat = MemoryLayout.size(ofValue: float)
        let vec2: vector_float2 = vector2(0, 0)
        let sizeOfVecFloat = MemoryLayout.size(ofValue: vec2)
        
        let numRowsDbl = Double(numRowsNS)
        let numColsDbl = Double(numColsNS)
        let scale: Double = Double(vertScale)
        
        var sources = [SCNGeometrySource]()
        var elements = [SCNGeometryElement]()
        
        let maxElements: Int = numRowsNS * numColsNS * 4
        var vertices = [SCNVector3](repeating:SCNVector3Zero, count:maxElements)
        var normals = [SCNVector3](repeating:SCNVector3Zero, count:maxElements)
         var uvList: [vector_float2] = []
        
        var vertexCount = 0
        let delta = 0.5
        
        for row in 0...numRowsNS-2 {
            for col in 0...numColsNS-1 {
                let x = Double(col)
                let y = Double(row)
                
                let NWZ = heightAt(x: col, y: row+1) / scale
                let NEZ = heightAt(x: col+1, y: row+1) / scale
                let SWZ = heightAt(x: col, y: row) / scale
                let SEZ = heightAt(x: col+1, y: row) / scale
                
                let NW = SCNVector3(x-delta, y+delta, NWZ)
                let NE = SCNVector3(x+delta, y+delta, NEZ)
                let SW = SCNVector3(x-delta, y-delta, SWZ)
                let SE = SCNVector3(x+delta, y-delta, SEZ)
                
                vertices[vertexCount] = SW
                vertices[vertexCount+1] = NW
                vertices[vertexCount+2] = NE
                vertices[vertexCount+3] = SE
                
                uvList.append(SCNVector2MakeDbl(x/numColsDbl, y/numRowsDbl))
                uvList.append(SCNVector2MakeDbl(x/numColsDbl, (y+delta)/numRowsDbl))
                uvList.append(SCNVector2MakeDbl((x+delta)/numColsDbl, (y+delta)/numRowsDbl))
                uvList.append(SCNVector2MakeDbl((x+delta)/numColsDbl, y/numRowsDbl))
                
                vertexCount += 4
            }
        }

        let source = SCNGeometrySource(vertices: vertices)
        sources.append(source)
        
        let geometryData = NSMutableData()
        
        var geometry: CInt = 0
        while (geometry < CInt(vertexCount)) {
            let bytes: [CInt] = [geometry, geometry+2, geometry+3, geometry, geometry+1, geometry+2]
            geometryData.append(bytes, length: sizeOfCInt*6)
            geometry += 4
        }
        
        let element = SCNGeometryElement(data: geometryData as Data, primitiveType: .triangles, primitiveCount: vertexCount/2, bytesPerIndex: sizeOfCInt)
        elements.append(element)
        
        for normalIndex in 0...vertexCount-1 {
            normals[normalIndex] = SCNVector3Make(0, 0, -1)
        }
        sources.append(SCNGeometrySource(normals: normals))
        
        let uvData = NSData(bytes: uvList, length: uvList.count * sizeOfVecFloat)
        let uvSource = SCNGeometrySource(data: uvData as Data, semantic: SCNGeometrySource.Semantic.texcoord, vectorCount: uvList.count, usesFloatComponents: true, componentsPerVector: 2, bytesPerComponent: sizeOfFloat, dataOffset: 0, dataStride: sizeOfVecFloat)
         sources.append(uvSource)
        
        meshGeometry = SCNGeometry(sources: sources, elements: elements)
        
        return meshGeometry
    }
    
    func create(withColor color: UIColor = UIColor.darkGray) {
        let terrainNode = SCNNode(geometry: createGeometry())
        self.addChildNode(terrainNode)
        
        self.color = color
    }
    
    required init(coder: NSCoder) { fatalError("Not implemented") }
}
