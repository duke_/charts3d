//
//  UIView+ViewAction.swift
//
//  Created by Duke Browning on 7/12/17.
//  Copyright (c) 2017 Atomic Skeeter Software. All rights reserved.
//

import UIKit

extension UIView {
    
    @discardableResult func addViewAction(handler:  (()->Void)? ) -> ViewAction {
        return ViewAction(view: self, handler: handler)
    }
}
