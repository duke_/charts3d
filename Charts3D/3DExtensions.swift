//
//  SCNVector3Extension.swift
//
//  Created by Duke Browning on 11/25/18.
//  Copyright © 2018 Atomic Skeeter Software, LLC. All rights reserved.
//

import Foundation
import SceneKit

postfix operator °

protocol IntegerInitializable: ExpressibleByIntegerLiteral {
    init (_: Int)
}

extension Int: IntegerInitializable {
    postfix public static func °(lhs: Int) -> Double {
        return Double(lhs) * .pi / 180
    }
}

extension Double {
    postfix public static func °(lhs: Double) -> Double {
        return Double(lhs) * .pi / 180
    }
    var ftToM : Double { return self * 0.3048 }
    var mToFt : Double { return self / 0.3048 }
}

extension Int {
    var ftToM : Double { return Double(self) * 0.3048 }
    var mToFt : Double { return 1.0 / ftToM }
}

extension CGPoint {
    func toVector3(z : CGFloat = 0.0) -> SCNVector3 {
        return SCNVector3(x, y, z)
    }
}

extension SCNVector3 {
    var zeroZ : SCNVector3 { return SCNVector3(x,y,0) }
    var xyz : (Double,Double,Double) { return (Double(x), Double(y), Double(z)) }
    
    func adding(x: Double, y: Double, z: Double) -> SCNVector3 {
        let (x0,y0,z0) = self.xyz
        return SCNVector3(x + x0, y + y0, z + z0)
    }
}

extension SCNNode {
    
    func addLine(pt1 : SCNVector3, pt2 : SCNVector3, color : UIColor = UIColor.white) {
        let pts = [pt1, pt2]
        
        let indices: [Int32] = [0, 1]
        
        let source = SCNGeometrySource(vertices: pts)
        let element = SCNGeometryElement(indices: indices, primitiveType: .line)
        let geometry = SCNGeometry(sources: [source], elements: [element])
        let node = SCNNode(geometry: geometry)
        let material = SCNMaterial()
        material.diffuse.contents = color
        geometry.materials = [material]
        addChildNode(node)
    }

}
