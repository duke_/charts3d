//
//  NoiseGenerator.swift
//
// Perlin Noise Generator
//
//  Created by Duke Browning on 11/23/18.
//  Copyright © 2018 Atomic Skeeter Software, LLC. All rights reserved.
//

import Foundation

class NoiseGenerator : ElevationProvider {
    private static let noiseX = 1619
    private static let noiseY = 31337
    private static let noiseSeed = 1013
    
    static var scale = 400.0
    static var offset = 0.0
    
    private var seed: Int = 1

    init() {
        seed = Int(arc4random()) % Int(INT32_MAX)
    }

    func valueFor(x: Int, y: Int) -> Double? {
        let octaves = 2
        let p = 0.5
        let zoom = 6.0
        var result = 0.0
        
        for a in 0..<octaves-1 {
            let frequency = pow(2, Double(a))
            let amplitude = pow(p, Double(a))
            
            result += noise(x:(Double(x))*frequency/zoom, y:(Double(y))/zoom*frequency)*amplitude
        }
        
        // result is value between -1 and 1

        return result * NoiseGenerator.scale + NoiseGenerator.offset
    }

    private func interpolate(a: Double, b: Double, x: Double) -> Double {
        let ft: Double = x * Double.pi
        let f: Double = (1.0-cos(ft)) * 0.5
        
        return a*(1.0-f)+b*f
    }

    private func findNoise(x: Double, y: Double) -> Double {
        var n = (NoiseGenerator.noiseX*Int(x) +
            NoiseGenerator.noiseY*Int(y) +
            NoiseGenerator.noiseSeed * seed) & 0x7fffffff
        
        n = (n >> 13) ^ n
        n = (n &* (n &* n &* 60493 + 19990303) + 1376312589) & 0x7fffffff
        
        return 1.0 - Double(n)/1073741824
    }
    
    private func noise(x: Double, y: Double) -> Double {
        let floorX: Double = Double(Int(x))
        let floorY: Double = Double(Int(y))
        
        let s = findNoise(x:floorX, y:floorY)
        let t = findNoise(x:floorX+1, y:floorY)
        let u = findNoise(x:floorX, y:floorY+1)
        let v = findNoise(x:floorX+1, y:floorY+1)
        
        let i1 = interpolate(a:s, b:t, x:x-floorX)
        let i2 = interpolate(a:u, b:v, x:x-floorX)
        
        return interpolate(a:i1, b:i2, x:y-floorY)
    }
    
}
