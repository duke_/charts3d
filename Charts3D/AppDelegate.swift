//
//  AppDelegate.swift
//
//  Created by Duke Browning on 11/23/18.
//  Copyright © 2018 Atomic Skeeter Software, LLC. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    private var _window: UIWindow?
    private var _mainViewController: AirportViewController?
    
    // ------------------------------------------------------------------------------
    
    func applicationDidFinishLaunching(_ application: UIApplication) {
        _mainViewController = AirportViewController()
        
        _window = UIWindow(frame: UIScreen.main.bounds)
        _window?.rootViewController = _mainViewController
        _window?.makeKeyAndVisible()
    }
    
    // ------------------------------------------------------------------------------
    
//    func applicationWillResignActive(_ application: UIApplication) {}
//    func applicationDidEnterBackground(_ application: UIApplication) {}
//    func applicationWillEnterForeground(_ application: UIApplication) {}
//    func applicationDidBecomeActive(_ application: UIApplication) {}
//    func applicationWillTerminate(_ application: UIApplication) {}
    
    // ------------------------------------------------------------------------------
    
}

