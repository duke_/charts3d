//
//  Ownship.swift
//
//  Created by Duke Browning on 11/23/18.
//  Copyright © 2018 Atomic Skeeter Software, LLC. All rights reserved.
//

import SceneKit

class Ownship : SCNNode {
    
    private let lookAtPosition = SCNVector3Make(0.0, 0.0, 0.0)
    // private let cameraPosition = SCNVector3(x: 0.8, y: 1, z: -0.5)
    private let cameraPosition = SCNVector3(x: 0.0, y: 0.0, z: 0)

    override init() {
        super.init()
        
        let cubeGeometry = SCNBox(width: 0.5, height: 0.5, length: 0.5, chamferRadius: 0.0)
        let ownshipNode = SCNNode(geometry: cubeGeometry)
        ownshipNode.isHidden = true
        addChildNode(ownshipNode)
        
        let colorMaterial = SCNMaterial()
        cubeGeometry.materials = [colorMaterial]
        
//        let lookAtNode = SCNNode()
//        lookAtNode.position = lookAtPosition
//        addChildNode(lookAtNode)
//
//        let cameraNode = SCNNode()
//        cameraNode.camera = SCNCamera()
//        cameraNode.position = cameraPosition
//        cameraNode.camera!.zNear = 0.1
//        cameraNode.camera!.zFar = 200
//        self.addChildNode(cameraNode)
//
//        let constraint1 = SCNLookAtConstraint(target: lookAtNode)
//        constraint1.isGimbalLockEnabled = true
//        cameraNode.constraints = [constraint1]
//
        // Create a spotlight at the ownship
//        let spotLight = SCNLight()
//        spotLight.type = SCNLight.LightType.spot
//        spotLight.spotInnerAngle = 40.0
//        spotLight.spotOuterAngle = 80.0
//        spotLight.castsShadow = true
//        spotLight.color = UIColor.white
//        let spotLightNode = SCNNode()
//        spotLightNode.light = spotLight
//        spotLightNode.position = SCNVector3(x: 1.0, y: 5.0, z: -2.0)
//        self.addChildNode(spotLightNode)
//
//        let constraint2 = SCNLookAtConstraint(target: self)
//        constraint2.isGimbalLockEnabled = true
//        spotLightNode.constraints = [constraint2]
        
        // Create additional omni light
//        let lightNode = SCNNode()
//        lightNode.light = SCNLight()
//        lightNode.light!.type = SCNLight.LightType.omni
//        lightNode.light!.color = UIColor.darkGray
//        lightNode.position = SCNVector3(x: 0, y: 10.00, z: -2)
//        self.addChildNode(lightNode)
    }
    
    required init(coder: NSCoder) { fatalError("Not implemented.") }
}
