//
//  ViewAction.swift
//
//  Created by Duke Browning on 7/12/17.
//  Copyright (c) 2017 Atomic Skeeter Software. All rights reserved.
//

import UIKit

class ViewAction {
    
    static let debugHighlighting = false // set to true to show hit bounds of all views with ViewActions added
    
    weak var view : UIView!
    var buttonOverlay : UIButton = UIButton(type: .custom)
    var handler : (()->Void)?
    
    var initialColor : UIColor? // user can set this value to override the view's background color on tapDown
    
    private var nonHighlightedColor : UIColor? // color to set back to
    private var userHliteColor : UIColor? // explictly set by user
    
    let ViewActionAssociatedObjectKey = UnsafeMutablePointer<Int8>.allocate(capacity: 1)
    
    var disabled = false {
        didSet {
            view?.isUserInteractionEnabled = !disabled
        }
    }
    
    private var pretintColors = [UIView:UIColor]()
    var tintColor : UIColor?
    var highlightDisabled = false
    
    var highlightedColor : UIColor? {
        get {
            guard view != nil else { return nil }
            
            if userHliteColor != nil {
                return userHliteColor!
            }
            
            return determineHliteColor()
        }
        
        set {
            userHliteColor = newValue
        }
    }
    
    init(view : UIView, handler:  (() -> Void)? ) {
        
        self.view = view
        self.handler = handler
        
        objc_setAssociatedObject(view, ViewActionAssociatedObjectKey, self, .OBJC_ASSOCIATION_RETAIN)
        
        view.isUserInteractionEnabled = true
        
        buttonOverlay.translatesAutoresizingMaskIntoConstraints = false
        buttonOverlay.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        buttonOverlay.addTarget(self, action: #selector(buttonUpOutside), for: .touchUpOutside)
        buttonOverlay.addTarget(self, action: #selector(buttonDown), for: .touchDown)
        
        view.addSubview(buttonOverlay)
        
        if ViewAction.debugHighlighting {
            buttonOverlay.backgroundColor = UIColor.yellow.withAlphaComponent(0.3)
        }
        
        constrainButtonOverlay()
    }
    
    private func constrainButtonOverlay() {
        leadingPaddingConstraint = buttonOverlay.constrainLeadingToSuperview(withInset: leadingHitPadding)
        trailingPaddingConstraint = buttonOverlay.constrainTrailingToSuperview(withInset: -trailingHitPadding)
        topPaddingConstraint = buttonOverlay.constrainTopToSuperview(withInset: -topHitPadding)
        bottomPaddingConstraint = buttonOverlay.constrainBottomToSuperview(withInset: -bottomHitPadding)
    }
    
    // If the view adds more views that could intercept the tap, this needs to be called after those
    // views have been added. It just puts the button back on top.
    func restack() {
        buttonOverlay.removeFromSuperview()
        view.addSubview(buttonOverlay)
        constrainButtonOverlay()
    }
    
    func shouldHighlight() -> Bool {
        
        if highlightDisabled { return false }
        
        return highlightedColor != nil || tintColor != nil
    }
    
    @objc
    func buttonDown() {
        
        if !shouldHighlight() {
            nonHighlightedColor = nil
            return
        }
        
        nonHighlightedColor = initialColor ?? view?.backgroundColor
        
        view.backgroundColor = highlightedColor

        tint()
    }
    
    @objc
    func buttonTapped() {
        if nonHighlightedColor != nil {
            view.backgroundColor = nonHighlightedColor!
        }
        nonHighlightedColor = nil

        untint()
        handler?()
    }
    
    @objc
    func buttonUpOutside() {
        if nonHighlightedColor != nil {
            view.backgroundColor = nonHighlightedColor!
        }
        nonHighlightedColor = nil
        untint()
    }
    
    func determineHliteColor() -> UIColor? {
        
        guard view != nil else { return nil }
        
        guard let baseColor = initialColor ?? view!.backgroundColor else { return nil }
        
        if baseColor == UIColor.clear { return nil }
        
        var h : CGFloat = 0
        var b : CGFloat = 0
        var s : CGFloat = 0
        var a : CGFloat = 0
        if baseColor.getHue(&h, saturation: &s, brightness: &b, alpha: &a) {
            
            if !(h == 0 && b == 0 && s == 0 && a == 0) { // this is what gets returned for white
                
                if a == 0 { return nil }
                
                let delt : CGFloat = b < 0.2 ? 0.1 : -0.1
                return UIColor(hue: h, saturation: s, brightness: max(0,min(1, b + delt)), alpha: a)
            }
        }
        
        return UIColor(white: 0.96, alpha: 1.0)
    }
    
    private func tint(view : UIView? = nil) {
        
        guard tintColor != nil else { return }
        
        var v : UIView!
        
        if view == nil {
            v = self.view
            pretintColors = [UIView:UIColor]() // reset at root
        }
        else {
            v = view
        }
        
        if let tintable = v as? UIImageView {
            pretintColors[v] = tintable.tintColor
            tintable.tintColor = tintColor!
        }
        else {
            for subview in v.subviews {
                self.tint(view: subview)
            }
        }
    }
    
    func untint() {
        
        guard !pretintColors.isEmpty else { return }
        
        UIView.animate(withDuration: 0.5) { [weak self] in
            guard let this = self else { return }
            guard self != nil else { return }
            for (v, color) in this.pretintColors {
                v.tintColor = color
            }
        }
    }
    
    // hit padding mundanery
    // you can specify extra padding so even though the view doesn't, the "button" still satisfies the standard
    // 44pt Apple-recommended minimum hit target
    
    var topHitPadding : CGFloat = 0 {
        didSet {
            topPaddingConstraint?.constant = -topHitPadding
        }
    }
    var bottomHitPadding : CGFloat = 0 {
        didSet {
            bottomPaddingConstraint?.constant = bottomHitPadding
        }
    }
    var leadingHitPadding : CGFloat = 0 {
        didSet {
            leadingPaddingConstraint?.constant = -leadingHitPadding
        }
    }
    var trailingHitPadding : CGFloat = 0 {
        didSet {
            trailingPaddingConstraint?.constant = trailingHitPadding
        }
    }
    
    var hitPadding : CGFloat = 0 {
        didSet {
            horzHitPadding = hitPadding
            vertHitPadding = hitPadding
        }
    }
    var horzHitPadding : CGFloat = 0 {
        didSet {
            leadingHitPadding = horzHitPadding
            trailingHitPadding = horzHitPadding
        }
    }
    var vertHitPadding : CGFloat = 0 {
        didSet {
            topHitPadding = vertHitPadding
            bottomHitPadding = vertHitPadding
        }
    }
    
    var leadingPaddingConstraint : NSLayoutConstraint?
    var trailingPaddingConstraint : NSLayoutConstraint?
    var topPaddingConstraint : NSLayoutConstraint?
    var bottomPaddingConstraint : NSLayoutConstraint?
    
}
