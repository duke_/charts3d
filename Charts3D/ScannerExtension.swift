//
//  ScannerExtension.swift
//  Bookmobile
//
//  Created by Duke Browning on 2/2/18.
//  Copyright © 2018 Atomic Skeeter Software. All rights reserved.
//

import Foundation

extension Scanner {
    
    func remaining() -> String {
        let s : String = string
        let loc = s.index(s.startIndex, offsetBy: scanLocation)
        return String((string as String)[loc...])
    }
    
    // return all characters up to the passed string
    // differences with NSScanner behavior:
    //   -- return empty string if scanner's current location starts with passed string
    //   -- returns nil if the passed string does not exist in remainder of scanner's string
    //   in both these cases, the scanner's location does not move.
    @discardableResult func scan(to subject: String) -> String? {
        
        let remainder = remaining()
        
        if remainder.hasPrefix(subject) { return "" }
        
        if !remainder.contains(subject) { return nil }
        
        var dataString : NSString?
        self.scanUpTo(subject, into: &dataString)
        
        return dataString as String?
    }
    
    func scanToEnd() -> String? {
        
        let remainder = remaining()

        if remainder.isEmpty { return nil }
        
        return remainder
    }
    
    // return all characters from current location to end of passed string, or nil if not found
    @discardableResult func scan(past subject : String) -> String? {
        
        let preStr = scan(to: subject)
        if preStr == nil { return nil }
        
        var dataString : NSString?
        if scanString(subject, into: &dataString) {
            if let str = dataString {
                let strng = str as String
                return preStr! + strng
            }
        }
        
        return nil
    }
    
    // move to the beginning of first/next occurrence of passed string
    @discardableResult func move(to subject : String) -> Bool {
        return scan(to: subject) != nil
    }
    
    // move to end of first/next occurrence of passed string
    @discardableResult func move(past subject : String) -> Bool {
        return scan(past: subject) != nil
    }
}
