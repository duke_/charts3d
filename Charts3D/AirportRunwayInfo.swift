//
//  AirportRunway.swift
//
//  Created by Duke Browning on 11/27/18.
//  Copyright © 2018 Atomic Skeeter Software, LLC. All rights reserved.
//

import Foundation

protocol AirportRunwayInfoProvider {
    var runwayInfo : AirportRunwayInfo { get }
}

class AirportRunwayInfo {
    
    // all dimensions are in Feet unless otherwise noted

    static var forAirport = [String:AirportRunwayInfo]()
    
    class Runway {

        class End {
            init(name : String, degreeMarker : Double, elevation : Double? = nil) {
                self.name = name
                self.degreeMarker = degreeMarker
                self.elevation = elevation
            }
            let name : String
            let degreeMarker : Double
            var elevation : Double? // uses airport elevation if missing
            var touchDownCoord : (Double, Double)? = nil
        }

        static let defaultLength = 5000.0
        static let defaultWidth = 100.0
        
        var length : Double
        var width : Double = Runway.defaultWidth
        
        var end1 : End
        var end2 : End
        
        var isSelected = false
        var selectedEnd : End
        
        var bounds : CGRect?
        
        var chartPositions = [String:(Double,Double)]()
        var imageLayerKeysByChartName = [String:String]()
        
        init(end1 : End, end2 : End, length : Double, width : Double? = nil, isSelected : Bool = false, end2Selected : Bool = false) {
            self.end1 = end1
            self.end2 = end2
            self.length  = length
            self.isSelected = isSelected
            self.selectedEnd = end2Selected ? end2 : end1
            if width != nil { self.width = width! }
        }
    }
    
    let airportCode : String
    
    var runwaysByName = [String : Runway]() // two entries per runway (each end)
    var runways = [Runway]()

    var selectedRunway : Runway?
    var selectedRunwayEnd : Runway.End? {
        return selectedRunway?.selectedEnd
    }

    init(airportCode : String, runways : [Runway]? = nil) {
        self.airportCode = airportCode
        if let rways = runways { for runway in rways { add(runway: runway) } }
        AirportRunwayInfo.forAirport[airportCode] = self
    }
    
    func add(runway : Runway) {
        runways.append(runway)
        runwaysByName[runway.end1.name] = runway
        runwaysByName[runway.end2.name] = runway
    }
    
    func select(runway endName: String?) {
        for runway in runways {
            if runway.end1.name == endName {
                selectedRunway = runway
                runway.selectedEnd = runway.end1
            }
            else if runway.end2.name == endName {
                selectedRunway = runway
                runway.selectedEnd = runway.end2
            }
            else {
                selectedRunway = nil
            }
        }
    }
}
