//
//  TerrainGrid.swift
//  Terrain3D
//
//  Created by Duke Browning on 12/3/18.
//  Copyright © 2018 Atomic Skeeter Software, LLC. All rights reserved.
//

import Foundation
import SceneKit

class TerrainGrid: SCNNode {

    let numRowsNS : Int
    let numColsEW : Int
    let interval : Double
    let heightProvider : (Int, Int) -> Double?
    
    init(rowsNS: Int = 32, colsEW: Int = 32, interval : Double = 100.0, elevationProvider : @escaping (Int, Int) -> Double? ) {
        self.numRowsNS = rowsNS
        self.numColsEW = colsEW
        self.interval = interval
        self.heightProvider = elevationProvider
        super.init()

        for row in 0..<numRowsNS {
           for colInv in 0..<numColsEW {
                let col = (numColsEW - colInv) - 1
                let x = Double(col) * interval
                let y = Double(row) * interval
                
                let NWZ = heightAt(x: col, y: row+1)
                let NEZ = heightAt(x: col+1, y: row+1)
                let SWZ = heightAt(x: col, y: row)
                let SEZ = heightAt(x: col+1, y: row)
                
                if NWZ == nil || NEZ == nil || SWZ == nil || SEZ == nil { continue }
                
                let SW = SCNVector3(x, y, SWZ!)
                let NW = SCNVector3(x, y+interval, NWZ!)
                let NE = SCNVector3(x+interval, y+interval, NEZ!)
                let SE = SCNVector3(x+interval, y, SEZ!)
                
                if col == 0 { addLine(pt1: SW, pt2: NW, color: UIColor.gray) }
                addLine(pt1: NW, pt2: NE, color: UIColor.gray)
                addLine(pt1: NE, pt2: SE, color: UIColor.gray)
                if row == 0 { addLine(pt1: SE, pt2: SW, color: UIColor.gray) }
            }
        }
    }
    
    func heightAt(x: Int, y: Int) -> Double? {
        return heightProvider(x, y)
    }
    
    required init(coder: NSCoder) { fatalError("Not implemented") }
}
