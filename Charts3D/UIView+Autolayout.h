//
//  UIView+Autolayout.h
//  Bookmobile
//
//  Created by Duke Browning on 7/30/17.
//  Copyright © 2017 Atomic Skeeter Software. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (Autolayout)

+ (instancetype) autolayout;
- (instancetype) autolayout;


@end

NS_ASSUME_NONNULL_END
