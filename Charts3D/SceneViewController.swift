//
//  AirportViewController.swift
//
//  Created by Duke Browning on 11/23/18.
//  Copyright © 2018 Atomic Skeeter Software, LLC. All rights reserved.
//

import UIKit
import SceneKit

class AirportViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view = ContainerView.sngl
        ContainerView.sngl.select(viewMode: .original2D)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .all
    }

}

class ContainerView : UIView {
    
    static let sngl = ContainerView()
    
    var viewMode : MainScene.ViewMode = .original2D
    
    let baseView = UIView.autolayout()
    
    var viewModeGroup = UIView.autolayout()
    
    init() {
        super.init(frame: CGRect.zero)
        translatesAutoresizingMaskIntoConstraints = false
        baseView.backgroundColor = UIColor.black
        addSubview(baseView)
        
        addControls()
        
        setNeedsUpdateConstraints()
    }
    
    var viewsByViewMode = [MainScene.ViewMode:UIView]()
    let orderedKeys = ["ORIG", "ML", "3D", "PLATE", "APPR", "FLY"]
    let viewModesByKey : [String:MainScene.ViewMode] = ["ORIG":.original2D, "ML":.decomposed, "3D":.taxi, "PLATE":.plate, "APPR":.approach, "FLY":.fly]
    
    private func viewModeSelectionChange(toggle : OnOffView, isOn : Bool) {
        if let rgKey = toggle.radioGroupKey, let toggles = OnOffView.forRadioGroupKey[rgKey] {
            if rgKey == "viewMode" {
                for t in toggles {
                    if t.key == toggle.key {
                        select(viewMode: viewModesByKey[t.key]!)
                    }
                    else {
                        t.isOn = false
                    }
                }
                
            }
        }
    }
    
    @discardableResult func select(viewMode : MainScene.ViewMode) -> UIView {
        
        var deferBySecs = 0
        
        var view = viewsByViewMode[viewMode]
        if view == nil {
            
            switch viewMode {
            case .original2D:
                view = ChartView()
            case .decomposed:
                view = DecomposedView()
            case .taxi:
                view = Airport3DView()
            case .plate:
                view = ApprChartView()
            case .approach:
                if self.viewMode == .plate {
                    if let plateView = viewsByViewMode[self.viewMode] as? ApprChartView {
                        deferBySecs = 5
                        plateView.leave()
                    }
                }
                view = OrbitView()
            case .fly:
                view = FlyingView()
            }
            
            view!.isHidden = true
            viewsByViewMode[viewMode] = view!
            baseView.addSubview(view!)
            view!.constrainAllSidesToSuperview()
        }
        
        if deferBySecs > 0 {
            DispatchQueue.main.asyncAfter(deadline: deferBySecs.seconds.fromNow) { [weak self] in
                guard let this = self else { return }
                this.switchTo(viewMode: viewMode)
            }
        }
        else {
            switchTo(viewMode: viewMode)
        }
        
        return view!
    }
    
    func switchTo(viewMode : MainScene.ViewMode) {
        for vm in viewModesByKey.values {
            if let v = viewsByViewMode[vm] {
                v.isHidden = vm != viewMode
            }
        }
    
        if self.viewMode == .plate {
            if let plateView = viewsByViewMode[self.viewMode] as? ApprChartView {
                plateView.reset()
            }
        }

        self.viewMode = viewMode
    }
    
    func addControls() {
        
        viewModeGroup = UIView.autolayout()
        viewModeGroup.backgroundColor = UIColor.black
        viewModeGroup.layer.cornerRadius = 4
        addSubview(viewModeGroup)

        var first = true
        for key in orderedKeys {
            let toggle = OnOffView(text: key, isOn: first, radioGroup: "viewMode", handler : viewModeSelectionChange)
            viewModeGroup.addSubview(toggle)
            first = false
        }
    }
    
    var constraintsCreated = false
    func setupConstraints() {
        constraintsCreated = true
        
        baseView.constrainAllSidesToSuperview()
        viewModeGroup.constrainTopToSuperview(withInset: 5)
        viewModeGroup.constrainLeadingToSuperview(withInset: 5)
        viewModeGroup.constrainSubviews(asRow: true, spacing: 10, startMargin: 5, endMargin: 5)
    }
    
    override func updateConstraints() {
        if !constraintsCreated { setupConstraints() }
        
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}

//////////////////////////////////////////////////////////////////////////

class ChartView : UIView {
    
    let baseView = UIView.autolayout()
    let imageView = UIImageView.autolayout()

    init() {
        super.init(frame: CGRect.zero)
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(baseView)
        
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "KDCA_00443AD")
        baseView.addSubview(imageView)
        
        setNeedsUpdateConstraints()
    }

    var constraintsCreated = false
    func setupConstraints() {
        constraintsCreated = true
        
        baseView.constrainAllSidesToSuperview()
        imageView.constrainAllSidesToSuperview()
    }
    
    override func updateConstraints() {
        if !constraintsCreated { setupConstraints() }
        
        super.updateConstraints()
    }

    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}

//////////////////////////////////////////////////////////////////////////

class ApprChartView : UIView {
    
    var mainScene : MainScene!
    let cameraNode = SCNNode()
    let camera = SCNCamera()
    let lookAtNode = SCNNode()
    var lookAtZeroConstraint : SCNLookAtConstraint!
    
    var initialPosition = SCNVector3(0, -100, 68000)
    let moveToPosition = SCNVector3(10000, -20000, 8000)

    let baseView = UIView.autolayout()
    let imageView = UIImageView.autolayout()
    let sceneView = SCNView.autolayout()
    
    var constraint : NSLayoutConstraint?
    
    init() {
        super.init(frame: CGRect.zero)
        translatesAutoresizingMaskIntoConstraints = false
        baseView.backgroundColor = UIColor.black
        addSubview(baseView)
        
        baseView.addSubview(imageView)

        mainScene = MainScene(viewMode: .plate)
        sceneView.scene = mainScene
        sceneView.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        sceneView.isHidden = true
        baseView.addSubview(sceneView)
        
        imageView.image = UIImage(named: "KDCA_LDA_Z_Rwy19_Full")
        imageView.contentMode = .scaleAspectFit
        
        setupCameras()
        mainScene.rootNode.eulerAngles = SCNVector3(-90°, 0, 0)
        
        setNeedsUpdateConstraints()
    }
    
    func reset() {
        moveToInitialPosition()
    }
    
    func leave() {

        constraint?.constant = 80
        imageView.animateConstraintChanges(duration: 1.0, completion: {[weak self] finished in
            guard let this = self else { return }
            this.sceneView.isHidden = false
            this.imageView.isHidden = true
            this.moveTo(position: this.moveToPosition, duration: 3.0)
        })
    }
    
    func setupCameras() {
        
        lookAtNode.position = SCNVector3(0,0,1) // center of chart
        mainScene.rootNode.addChildNode(lookAtNode)
        
        cameraNode.camera = camera
        cameraNode.position = initialPosition
        camera.automaticallyAdjustsZRange = true
        turnOnCameraControls()
        mainScene.rootNode.addChildNode(cameraNode)
    }
    
    func turnOnCameraControls() {
        lookAtZeroConstraint = SCNLookAtConstraint(target: lookAtNode)
        lookAtZeroConstraint.isGimbalLockEnabled = true
        cameraNode.constraints = [lookAtZeroConstraint]
    }
    
    func moveToInitialPosition(duration: TimeInterval = 0.0) {
        moveTo(position: initialPosition, duration: duration)
    }
    
    func moveTo(position : SCNVector3, duration: TimeInterval = 0.0) {
        if duration > 0.0 {
            cameraNode.runAction(SCNAction.move(to: position, duration: duration))
        }
    }
    
    var constraintsCreated = false
    func setupConstraints() {
        constraintsCreated = true
        
        baseView.constrainAllSidesToSuperview()
        sceneView.constrainAllSidesToSuperview()
        imageView.constrainWidth(to: UIScreen.main.bounds.width)
        imageView.constrainHeight(to: UIScreen.main.bounds.height)
        imageView.constrainXAlignedToSuperview()
        constraint = imageView.constrainYAlignedToSuperview()
    }
    
    override func updateConstraints() {
        if !constraintsCreated { setupConstraints() }
        
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}

///////////////////////////////////////////////////////////////////////////////

class DecomposedView : UIView {
    
    var mainScene : MainScene!
    let cameraNode = SCNNode()
    let camera = SCNCamera()
    let lookAtNode = SCNNode()
    var lookAtZeroConstraint : SCNLookAtConstraint!
    var currentAltitude : Float = 0.0
    
    var initialPosition = SCNVector3(0, -100, 2800)
    
    let baseView = UIView.autolayout()
    let sceneView = SCNView.autolayout()

    var taxiModeLabel = UILabel.autolayout()
    var approachModeLabel = UILabel.autolayout()
    
    var togglesByLayer = [String:OnOffView]()
    let layerFiltersView = UIView.autolayout()
    let modeView = UIView.autolayout()
    let typeView = UIView.autolayout()

    var mode = "All"
    var type = "Any"
    var layerKeys : [String] {
        
        if mode == "Arrival" {
            return ["Tarmac", "Structures", "RunwayInfo", "EMAS", "FieldInfo", "Instructions", "Obstacles", "Runways"]
        }

        if mode == "Taxi" {
            if type == "GA" {
                return  ["Runways", "Tarmac", "Structures", "Buildings", "HoldBays", "HotSpots", "Instructions", "Parking", "Taxiways"]
            }
            
            if type == "Airline" {
                return  ["Runways", "Tarmac", "Structures", "Gates", "HoldBays", "HotSpots", "Instructions", "Taxiways"]
            }

            return  ["Runways", "Tarmac", "Structures", "Buildings", "Gates", "HoldBays", "HotSpots", "Instructions", "Parking", "Taxiways"]
        }

        // Any type, all layers
        return SceneKDCA.sngl.airportDiagram.orderedLayers.map { $0.key }
    }

    init() {
        super.init(frame: CGRect.zero)
        translatesAutoresizingMaskIntoConstraints = false
        baseView.backgroundColor = UIColor.blue.withAlphaComponent(0.2)
        addSubview(baseView)
        
        mainScene = MainScene(viewMode: .decomposed)
        sceneView.scene = mainScene
        sceneView.backgroundColor = UIColor.black
        baseView.addSubview(sceneView)

        addControls()
        setupCameras()
        mainScene.rootNode.eulerAngles = SCNVector3(-90°, 0, 0)

        setNeedsUpdateConstraints()
    }
    
    func setupCameras() {
        
        lookAtNode.position = SCNVector3(0,0,1) // center of chart
        mainScene.rootNode.addChildNode(lookAtNode)
        
        cameraNode.camera = camera
        cameraNode.position = initialPosition
        camera.automaticallyAdjustsZRange = true
        turnOnCameraControls()
        mainScene.rootNode.addChildNode(cameraNode)
    }
    
    func turnOnCameraControls() {
        sceneView.allowsCameraControl = true
        sceneView.defaultCameraController.interactionMode = .truck
        sceneView.defaultCameraController.target = lookAtNode.position
        sceneView.defaultCameraController.maximumVerticalAngle = 89.0
        sceneView.defaultCameraController.minimumVerticalAngle = 87.0
        sceneView.defaultCameraController.inertiaEnabled = true
        sceneView.defaultCameraController.clearRoll()
        lookAtZeroConstraint = SCNLookAtConstraint(target: lookAtNode)
        lookAtZeroConstraint.isGimbalLockEnabled = true
        cameraNode.constraints = [lookAtZeroConstraint]
    }
    
    private func filterSelectionChange(toggle : OnOffView, isOn : Bool) {
        mainScene.updateVisibility(layerKey: toggle.key, to: isOn)
    }
    
    private func typeSelectionChange(toggle : OnOffView, isOn : Bool) {
        type = toggle.key
        displayLayers()
    }
    
    private func modeSelectionChange(toggle : OnOffView, isOn : Bool) {
        mode = toggle.key
        displayLayers()
    }
    
    private func displayLayers() {

        for layer in SceneKDCA.sngl.airportDiagram.orderedLayers {
            mainScene.updateVisibility(layerKey: layer.key, to: layerKeys.contains(layer.key))
            togglesByLayer[layer.key]!.isOn = layerKeys.contains(layer.key)
        }
    }
    
//    func moveToInitialPosition(duration: TimeInterval = 0.0) {
//        move(pov: cameraNode, to: initialPosition, duration: duration)
//    }
//
//    func move(pov : SCNNode, to position : SCNVector3, duration: TimeInterval = 0.0) {
//        if duration > 0.0 {
//            pov.runAction(SCNAction.move(to: position, duration: duration))
//        }
//    }
    
    func addControls() {
        
        baseView.addSubview(layerFiltersView)
        baseView.addSubview(modeView)
        baseView.addSubview(typeView)

        for layer in SceneKDCA.sngl.airportDiagram.orderedLayers {
            let toggle = OnOffView(text: layer.key, isOn : MainScene.layerVisibility[layer.key] ?? false, handler : filterSelectionChange)
            togglesByLayer[layer.key] = toggle
            layerFiltersView.addSubview(toggle)
        }
        
        modeView.addSubview(OnOffView(text: "All", isOn : true, radioGroup: "layerMode", handler : modeSelectionChange))
        modeView.addSubview(OnOffView(text: "Taxi", isOn : false, radioGroup: "layerMode", handler : modeSelectionChange))
        modeView.addSubview(OnOffView(text: "Arrival", isOn : false, radioGroup: "layerMode", handler : modeSelectionChange))

        typeView.addSubview(OnOffView(text: "Any", isOn : true, radioGroup: "typeMode", handler : typeSelectionChange))
        typeView.addSubview(OnOffView(text: "GA", isOn : false, radioGroup: "typeMode", handler : typeSelectionChange))
        typeView.addSubview(OnOffView(text: "Airline", isOn : false, radioGroup: "typeMode", handler : typeSelectionChange))
    }
    
    var constraintsCreated = false
    func setupConstraints() {
        constraintsCreated = true
        
        baseView.constrainAllSidesToSuperview()
        sceneView.constrainAllSidesToSuperview()
        
        layerFiltersView.constrainTopToSuperview(withInset: 50)
        layerFiltersView.constrainLeadingToSuperview(withInset: 20)
        layerFiltersView.constrainSubviews()

        modeView.constrainTopToSuperview(withInset: 50)
        modeView.constrainTrailingToSuperview(withInset: 20)
        modeView.constrainSubviews()

        typeView.constrainAsBelow(to: modeView, withOffset: 100)
        typeView.constrainTrailingToSuperview(withInset: 20)
        typeView.constrainSubviews()
    }
    
    override func updateConstraints() {
        if !constraintsCreated { setupConstraints() }
        
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}

class Airport3DView : UIView {
    
    var mainScene : MainScene!
    let cameraNode = SCNNode()
    let camera = SCNCamera()
    let lookAtNode = SCNNode()
    var lookAtZeroConstraint : SCNLookAtConstraint!
    
    var initialPosition = SCNVector3(0, -1000, 3000)
    
    let baseView = UIView.autolayout()
    let sceneView = SCNView.autolayout()
    
    var taxiwaysLabel = UILabel.autolayout()
    
    let taxiwayFiltersView = UIView.autolayout()
    
    init() {
        super.init(frame: CGRect.zero)
        translatesAutoresizingMaskIntoConstraints = false
        baseView.backgroundColor = UIColor.blue.withAlphaComponent(0.2)
        addSubview(baseView)
        
        mainScene = MainScene(viewMode: .taxi)
        sceneView.scene = mainScene
        sceneView.backgroundColor = UIColor.black
        baseView.addSubview(sceneView)
        
        taxiwaysLabel.text = "Taxiways"
        taxiwaysLabel.font = UIFont.boldSystemFont(ofSize: 12)
        taxiwaysLabel.textColor = .white
        baseView.addSubview(taxiwaysLabel)
        
        addControls()
        setupCameras()
        mainScene.rootNode.eulerAngles = SCNVector3(-90°, 0, 0)

        setNeedsUpdateConstraints()
    }
    
    func setupCameras() {
        
        lookAtNode.position = SCNVector3(0,0,1) // center of chart
        mainScene.rootNode.addChildNode(lookAtNode)
        
        cameraNode.camera = camera
        cameraNode.position = initialPosition
        camera.automaticallyAdjustsZRange = true
        turnOnCameraControls()
        mainScene.rootNode.addChildNode(cameraNode)
    }
    
    func turnOnCameraControls() {
        sceneView.allowsCameraControl = true
        sceneView.defaultCameraController.interactionMode = .orbitTurntable
        sceneView.defaultCameraController.target = lookAtNode.position
        sceneView.defaultCameraController.maximumVerticalAngle = 89.0
        sceneView.defaultCameraController.minimumVerticalAngle = 1.0
        sceneView.defaultCameraController.inertiaEnabled = true
        sceneView.defaultCameraController.clearRoll()
        lookAtZeroConstraint = SCNLookAtConstraint(target: lookAtNode)
        lookAtZeroConstraint.isGimbalLockEnabled = true
        cameraNode.constraints = [lookAtZeroConstraint]
    }
    
    func moveToInitialPosition(duration: TimeInterval = 0.0) {
        moveTo(position: initialPosition, duration: duration)
    }
    
    func moveTo(position : SCNVector3, duration: TimeInterval = 0.0) {
        if duration > 0.0 {
            cameraNode.runAction(SCNAction.move(to: position, duration: duration))
        }
    }

    func addControls() {
        
        baseView.addSubview(taxiwayFiltersView)
        
        for taxiway in SceneKDCA.sngl.airportDiagram.taxiways.keys.sorted() {
            addFilter(forTaxiway: taxiway)
        }
    }
    
    private func filterSelectionChange(toggle : OnOffView, isOn : Bool) {
        mainScene.updateTaxiwayVisibility(taxiway: toggle.key, to: toggle.isOn)
        
    }
    
    func addFilter(forTaxiway taxiway : String) {
        taxiwayFiltersView.addSubview(OnOffView(text: taxiway, isOn : MainScene.taxiwayVisibility[taxiway] ?? false, handler : filterSelectionChange))
    }
    
    var constraintsCreated = false
    func setupConstraints() {
        constraintsCreated = true
        
        baseView.constrainAllSidesToSuperview()
        sceneView.constrainAllSidesToSuperview()

        taxiwaysLabel.constrainTopToSuperview(withInset: 50)
        taxiwaysLabel.constrainLeadingToSuperview(withInset: 10)
        taxiwaysLabel.constrainHeight(to: 26)

        taxiwayFiltersView.constrainAsBelow(to: taxiwaysLabel, withOffset: 5)
        taxiwayFiltersView.constrainLeadingToSuperview(withInset: 20)
        taxiwayFiltersView.constrainSubviews()
    }
    
    override func updateConstraints() {
        if !constraintsCreated { setupConstraints() }
        
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}

class OrbitView : UIView {
    
    var mainScene : MainScene!
    let cameraNode = SCNNode()
    let camera = SCNCamera()
    let lookAtNode = SCNNode()
    var lookAtZeroConstraint : SCNLookAtConstraint!
    
    let initialPosition = SCNVector3(10000, -20000, 8000)
    
    let baseView = UIView.autolayout()
    let sceneView = SCNView.autolayout()
    let filtersView = UIView.autolayout()
    
    init() {
        super.init(frame: CGRect.zero)
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(baseView)
        
        mainScene = MainScene(viewMode: .approach)
        sceneView.scene = mainScene
        sceneView.backgroundColor = UIColor.black
        baseView.addSubview(sceneView)
        
        addControls()
        setupCameras()
        mainScene.rootNode.eulerAngles = SCNVector3(-90°, 0, 0)

        setNeedsUpdateConstraints()
    }
    
    func setupCameras() {
        
        lookAtNode.position = SCNVector3(0,0,1) // center of chart
        mainScene.rootNode.addChildNode(lookAtNode)
        
        cameraNode.camera = camera
        cameraNode.position = initialPosition
        camera.automaticallyAdjustsZRange = true
        turnOnCameraControls()
        mainScene.rootNode.addChildNode(cameraNode)
    }
    
    func turnOnCameraControls() {
        sceneView.allowsCameraControl = true
        sceneView.defaultCameraController.interactionMode = .orbitTurntable
        sceneView.defaultCameraController.target = lookAtNode.position
        sceneView.defaultCameraController.maximumVerticalAngle = 89.0
        sceneView.defaultCameraController.minimumVerticalAngle = 1.0
        sceneView.defaultCameraController.inertiaEnabled = true
        sceneView.defaultCameraController.clearRoll()
        lookAtZeroConstraint = SCNLookAtConstraint(target: lookAtNode)
        lookAtZeroConstraint.isGimbalLockEnabled = true
        cameraNode.constraints = [lookAtZeroConstraint]
    }
    
    func moveToInitialPosition(duration: TimeInterval = 0.0) {
        move(pov: cameraNode, to: initialPosition, duration: duration)
    }
    
    func move(pov : SCNNode, to position : SCNVector3, duration: TimeInterval = 0.0) {
        if duration > 0.0 {
            pov.runAction(SCNAction.move(to: position, duration: duration))
        }
    }
    
    func addControls() {
        
        baseView.addSubview(filtersView)
        
        filtersView.addSubview(OnOffView(text: "Chart", isOn : true, handler : filterSelectionChange))
        filtersView.addSubview(OnOffView(text: "Flight Path", isOn : false, handler : filterSelectionChange))
        filtersView.addSubview(OnOffView(text: "Final Vector", isOn : true, handler : filterSelectionChange))
        filtersView.addSubview(OnOffView(text: "Waypoints", isOn : true, handler : filterSelectionChange))
        filtersView.addSubview(OnOffView(text: "Missed Approach", isOn : true, handler : filterSelectionChange))
    }
    
    private func filterSelectionChange(toggle : OnOffView, isOn : Bool) {
        if toggle.key == "Chart" {
            mainScene.approachChartNode?.isHidden = !isOn
        }
        else if toggle.key == "Flight Path" {
            mainScene.flightPathNode?.isHidden = !isOn
        }
        else if toggle.key == "Final Vector" {
            mainScene.specifiedPathNode?.isHidden = !isOn
        }
        else if toggle.key == "Waypoints" {
            mainScene.labeledPtGroupNode?.isHidden = !isOn
        }
        else if toggle.key == "Missed Approach" {
            mainScene.missedApproachNode?.isHidden = !isOn
        }
    }
    
    func addFilter(forLayer layer : Chart.Layer) {
        filtersView.addSubview(OnOffView(text: layer.key, isOn : MainScene.layerVisibility[layer.key] ?? false, handler : filterSelectionChange))
    }
    
    var constraintsCreated = false
    func setupConstraints() {
        constraintsCreated = true
        
        baseView.constrainAllSidesToSuperview()
        sceneView.constrainAllSidesToSuperview()
        
        filtersView.constrainTopToSuperview(withInset: 50)
        filtersView.constrainLeadingToSuperview(withInset: 20)
        filtersView.constrainSubviews()
    }
    
    override func updateConstraints() {
        if !constraintsCreated { setupConstraints() }
        
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}

class FlyingView : UIView {
    
    var mainScene : MainScene!
    let cameraNode = SCNNode()
    let camera = SCNCamera()
    let lookAtNode = SCNNode()
    var lookAtZeroConstraint : SCNLookAtConstraint!
    var currentAltitude : Float = 0.0
    
    let initialPosition = SCNVector3(10000, -20000, 8000)
    
    let baseView = UIView.autolayout()
    let sceneView = SCNView.autolayout()
    var toggles = [String:UISwitch]()
    let toggleSet = UIView.autolayout()
    var flyButton = UIView.autolayout()
    var flyButtonLabel = UILabel.autolayout()
    var apprchPlateToggle : OnOffView?
    
    let layerFiltersView = UIView.autolayout()
    
    init() {
        super.init(frame: CGRect.zero)
        translatesAutoresizingMaskIntoConstraints = false
        baseView.backgroundColor = UIColor.blue.withAlphaComponent(0.2)
        addSubview(baseView)
        
        mainScene = MainScene(viewMode: .fly)
        sceneView.scene = mainScene
        sceneView.backgroundColor = UIColor.black
        baseView.addSubview(sceneView)
        
        addControls()
        setupCameras()
        mainScene.rootNode.eulerAngles = SCNVector3(-90°, 0, 0)

        setNeedsUpdateConstraints()
    }
    
    func setupCameras() {
        
        lookAtNode.position = SCNVector3(0,0,1) // center of chart
        mainScene.rootNode.addChildNode(lookAtNode)
        
        cameraNode.camera = camera
        cameraNode.position = initialPosition
        camera.automaticallyAdjustsZRange = true
        turnOnCameraControls()
        mainScene.rootNode.addChildNode(cameraNode)
    }
    
    func turnOnCameraControls() {
        lookAtZeroConstraint = SCNLookAtConstraint(target: lookAtNode)
        lookAtZeroConstraint.isGimbalLockEnabled = true
        cameraNode.constraints = [lookAtZeroConstraint]
    }
    
    func moveToInitialPosition(duration: TimeInterval = 0.0) {
        moveTo(position: initialPosition, duration: duration)
    }
    
    func moveTo(position : SCNVector3, duration: TimeInterval = 0.0) {
        if duration > 0.0 {
            cameraNode.runAction(SCNAction.move(to: position, duration: duration))
        }
        else {
            cameraNode.position = position
        }
    }
    
    func flyPath() {
        guard let groundTrack = SceneKDCA.sngl.groundTracks["flightPath"] else { return }
        let currentPosition = cameraNode.position
        let altitude = AltitudeProfile(points: [CGPoint(x: 0, y: 9000.0.ftToM), CGPoint(x: 0.9, y: 30.0), CGPoint(x: 1.0, y: 30.0)])
        let path = ThreeDPath(groundTrack: groundTrack.0, altitudeProfile : altitude, numPoints: 80)
        let pts = path.path.map { $0.adding(x: Double(groundTrack.1) * mainScene.approachChart.mPerPt,
                                            y: -Double(groundTrack.2) * mainScene.approachChart.mPerPt,
                                            z: 0.0)
        }
        moveTo(position: pts[0], duration: 1.0)
        var actions = [SCNAction]()
        for pt in pts {
            actions.append(SCNAction.move(to: pt, duration: 0.1))
        }
        DispatchQueue.main.asyncAfter(deadline: 2.seconds.fromNow) { [weak self] in
            guard let this = self else { return }
            this.lookAtNode.position = SCNVector3(0, -5000, 0)
            // let actions = path.actions(intervalDuration: 0.1)
            SCNTransaction.begin()
            SCNTransaction.animationDuration = Double(actions.count) * 0.1
            this.cameraNode.runAction(SCNAction.sequence(actions))
            SCNTransaction.commit()
            DispatchQueue.main.asyncAfter(deadline: 6.seconds.fromNow) { [weak self] in
                self?.moveTo(position: currentPosition)
            }
        }
    }

    func addControls() {
        
        baseView.addSubview(flyButton)
        flyButton.layer.cornerRadius = 3
        flyButton.layer.borderColor = UIColor.white.cgColor
        flyButton.layer.borderWidth = 2
        flyButton.addViewAction { [weak self] in
            ContainerView.sngl.select(viewMode: .fly)
            self?.flyPath() }
        flyButtonLabel.text = "Fly"
        flyButtonLabel.textColor = UIColor.white
        flyButton.addSubview(flyButtonLabel)
    }
    
    var constraintsCreated = false
    func setupConstraints() {
        constraintsCreated = true
        
        baseView.constrainAllSidesToSuperview()
        sceneView.constrainAllSidesToSuperview()
        
        flyButton.constrainWidth(to: 80)
        flyButton.constrainHeight(to: 40)
        flyButton.constrainTrailingToSuperview(withInset: 10)
        flyButton.constrainTopToSuperview(withInset: 10)
        flyButtonLabel.centerInSuperview()
    }
    
    override func updateConstraints() {
        if !constraintsCreated { setupConstraints() }
        
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}

