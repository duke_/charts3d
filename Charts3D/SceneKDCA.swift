//
//  SceneKDCA.swift
//
//  Created by Duke Browning on 11/29/18.
//  Copyright © 2018 Atomic Skeeter Software, LLC. All rights reserved.
//

import Foundation
import UIKit
import SceneKit

class SceneKDCA {
    
    static let sngl = SceneKDCA()
    static func select() { }
    
    init() {
        populateAirportInfo()
        populateRunwayInfo()
    }
    
    var groundTracks = [String:(UIBezierPath,Double,Double,Double?)]()
    var altitudeProfiles = [String:AltitudeProfile]()
    var touchDownPts = [String:CGPoint]()
    var landingTerminationPts = [String:CGPoint]()
    var imageNamesForImageKeys = [String:String]()

    func populateAirportInfo() {
        _ = AirportInfo(airportCode: "KDCA", latitude: 38.851444, longitude: 77.0377222, elevation: 4222.ftToM) // 14.1.ftToM)
        AirportInfo.selectedAirportCode = "KDCA"
        
    }

    func populateRunwayInfo() {
        let rw1end1 = AirportRunwayInfo.Runway.End(name: "19", degreeMarker: 186.5, elevation: 12)
        let rw1end2 = AirportRunwayInfo.Runway.End(name: "1", degreeMarker: 7.0, elevation: 11)
        let rw1 = AirportRunwayInfo.Runway.init(end1: rw1end1, end2: rw1end2, length: 7169, width: 150, isSelected: true)
        rw1.bounds = CGRect(x: 200.82, y: 44.07, width: 41.04, height: 412.3)
        rw1.chartPositions["19"] = (205,26)
        rw1.chartPositions["1"] = (241,484)

        let rw2end1 = AirportRunwayInfo.Runway.End(name: "15", degreeMarker: 154, elevation: 14)
        let rw2end2 = AirportRunwayInfo.Runway.End(name: "33", degreeMarker: 334, elevation: 10)
        let rw2 = AirportRunwayInfo.Runway.init(end1: rw2end1, end2: rw2end2, length: 5204, width: 150)
        rw2.bounds = CGRect(x: 126.73, y: 42.43, width: 188.71, height: 243.47)
        rw2.chartPositions["15"] = (117,37)
        rw2.chartPositions["33"] = (316,306)

        let rw3end1 = AirportRunwayInfo.Runway.End(name: "22", degreeMarker: 217, elevation: 11)
        let rw3end2 = AirportRunwayInfo.Runway.End(name: "4", degreeMarker: 37, elevation: 11)
        let rw3 = AirportRunwayInfo.Runway.init(end1: rw3end1, end2: rw3end2, length: 5000, width: 150)
        rw3.bounds = CGRect(x: 156.46, y: 203.66, width: 132.03, height: 263.5)
        rw3.chartPositions["22"] = (289,199)
        rw3.chartPositions["4"] = (159,481)

        _ = AirportRunwayInfo(airportCode: "KDCA", runways: [rw1,rw2,rw3])
    }
    
    func findTaxiways() -> TextPoints {
        
        let txtPts = ValuesFromPDF(named: "KDCA_PDFOut", offset: (-12.0, -46.0)).valueDict()

//        for (key,pts) in taxiways(fromTextPoints: txtPts) {
//            print("\(key):")
//            for pt in pts {
//                print("    \(Int(pt.0)),\(Int(pt.1))")
//            }
//        }
        
        return taxiways(fromTextPoints: txtPts)
        
    }
    
    func taxiways(fromTextPoints txtPts : TextPoints ) -> TextPoints {
        var result = TextPoints()
        for (text, pts) in txtPts {
            if isTaxiway(text: text) {
                result[text] = pts
            }
        }
        
        return result
    }
    
    func isTaxiway(text : String) -> Bool {
        if text.count < 1 || text.count > 2 { return false }
        let firstIndex = text.startIndex
        let firstChar = String(text[firstIndex])
        if !(  firstChar == "B"
            || firstChar == "E" || firstChar == "F" || firstChar == "G" || firstChar == "H"
            || firstChar == "I" || firstChar == "J" || firstChar == "K" || firstChar == "L"
            || firstChar == "M" || firstChar == "N" || firstChar == "O" || firstChar == "P"
            || firstChar == "Q" || firstChar == "S" || firstChar == "T"
            || firstChar == "U"
            || firstChar == "Y" || firstChar == "Z") { return false }
        // if firstChar.range(of: "[^A-Z]", options: .regularExpression) == nil { return false }
        if text.count == 2 {
            let secondIndex = text.index(firstIndex, offsetBy: 1)
            let secondChar = String(text[secondIndex])
            if !(secondChar == "1" || secondChar == "2" || secondChar == "3" || secondChar == "4"
                || secondChar == "5" || secondChar == "6" || secondChar == "7" || secondChar == "8"
                || secondChar == "9") { return false }
            // if secondChar.range(of: "[^1-9]", options: .regularExpression) == nil { return false }
        }
        
        return true
    }
    

    
    lazy var airportInfo : AirportDiagram = {
        let chart = AirportDiagram(code: "KDCA",
                           refImageName: "KDCA_Airport_Info",
                                  refPt: CGPoint(x: 349.0, y: 373.0),
                                 refLat: 38.0 + 51.0/60.0,
                                 refLng: -(77.0 + 2.0 / 60.0),
                         ptPerMinuteLat: 210.0,
                         ptPerMinuteLng: 330.0,
                              elevation: 14.ftToM)

        chart.refPts["VOR"] = (288.25, 134.5)
        
        return chart
    }()
    
    lazy var originalChart : AirportDiagram = {
        let chart = AirportDiagram(code: "KDCA",
                           refImageName: "KDCA_00443AD",
                                  refPt: CGPoint(x: 300.5, y: 350.2),
                                 refLat: 38.0 + 51.0/60.0,
                                 refLng: -(77.0 + 2.0 / 60.0),
                         ptPerMinuteLat: 350.0,
                         ptPerMinuteLng: 274.0,
                              elevation: 14.ftToM)
        
        return chart
    }()
    
    lazy var airportDiagram : AirportDiagram = {

        // TEMP NOTE: add 22 to y coordinates of pdf scan

        let chart = AirportDiagram(code: "KDCA",
                           refImageName: "KDCA_Runways",
                                  refPt: CGPoint(x: 157.0, y: 279.0),
                                 refLat: 38.0 + 51.0/60.0,
                                 refLng: -(77.0 + 2.5 / 60.0),
                         ptPerMinuteLat: 350.0,
                         ptPerMinuteLng: 274.0,
                              elevation: 14.ftToM)
        
        chart.imageNameProvider = { [weak self] key in
            guard let this = self else { return key }
            
            if let name = this.imageNamesForImageKeys[key] { return name }
            
            return "KDCA_" + key
        }
        
        chart.addLayer(withKey: "Runways")
        chart.addLayer(withKey: "Tarmac") //, color: UIColor.orange.withAlphaComponent(0.8))
        chart.addLayer(withKey: "Structures")
        chart.addLayer(withKey: "Buildings")
        chart.addLayer(withKey: "RunwayInfo")
        chart.addLayer(withKey: "EMAS")
        chart.addLayer(withKey: "FieldInfo")
        chart.addLayer(withKey: "FreqLegend")
        chart.addLayer(withKey: "Gates")
        chart.addLayer(withKey: "HoldBays")
        chart.addLayer(withKey: "HotSpots")
        chart.addLayer(withKey: "Instructions")
        chart.addLayer(withKey: "LatLng")
        chart.addLayer(withKey: "Obstacles")
        chart.addLayer(withKey: "Parking")
        chart.addLayer(withKey: "Taxiways")

        chart.imageKeysByLayerKey["Buildings"] = ["FireStation","Hangars","NWS","Terminals","Tower"]
        chart.imageKeysByLayerKey["Runways"] = ["Runway1-19","Runway4-22","Runway15-33"]
        chart.imageKeysByLayerKey["RunwayInfo"] = ["RunwayInfo1","RunwayInfo4","RunwayInfo15","RunwayInfo19","RunwayInfo22","RunwayInfo33"]
        chart.selectedRunwayKey = "Runway1-19"

        chart.refPts["VOR"] = (238.0, 83.0)
        chart.taxiways = findTaxiways()

        return chart
    }()
    
    lazy var approachLDA_Z_Rwy_19 : Chart = {
        let chart = Chart(code: "KDCA",
                  refImageName: "KDCA_LDA_Z_Rwy_19",
                         refPt: CGPoint(x: 277.0, y: 214.0),
                        refLat: 38.0 + 50.0 / 60.0,
                        refLng: -77.0,
                ptPerMinuteLat: 201.0 / 10.0,
                ptPerMinuteLng: 157.0 / 10.0)

        //chart.refPts["VOR"] = (242.0, 182.0)
        
        var key = "FERGI"
        chart.refPts[key] = (109,26)
        chart.refAlts[key] = 3000.0.ftToM
        chart.labeledPoints[key] = LabeledPoint(text: key, position: chart.refPointPosition(withKey: key)!)
        
        key = "HATAL"
        chart.refPts[key] = (150,71)
        chart.refAlts[key] = 2300.0.ftToM
        chart.labeledPoints[key] = LabeledPoint(text: key, position: chart.refPointPosition(withKey: key)!)
        
        key = "✠ BESSE"
        chart.refPts[key] = (178,100)
        chart.refAlts[key] = 1700.0.ftToM
        chart.labeledPoints[key] = LabeledPoint(text: key, position: chart.refPointPosition(withKey: key)!)
        
        key = "WEVPU"
        chart.refPts[key] = (207,130)
        chart.refAlts[key] = 1100.0.ftToM
        chart.labeledPoints[key] = LabeledPoint(text: key, position: chart.refPointPosition(withKey: key)!)
        
        key = "ZAXEB"
        chart.refPts[key] = (240,163)
        chart.refAlts[key] = 720.0.ftToM
        chart.labeledPoints[key] = LabeledPoint(text: key, position: chart.refPointPosition(withKey: key)!)
        
        key = "BADDN"
        chart.refPts[key] = (252,300)
        chart.refAlts[key] = 3000.0.ftToM
        let lpt = LabeledPoint(text: key, position: chart.refPointPosition(withKey: key)!)
        lpt.color = .red
        chart.labeledPoints[key] = lpt
        
        key = "AbortPoint"
        chart.refPts[key] = (240,179)
        chart.refAlts[key] = 500.0.ftToM

        chart.lines["FinalVector"] = Line(point1: chart.refPointPosition(withKey: "FERGI")!, point2: chart.refPointPosition(withKey: "ZAXEB")!, withGroundLine : true)
        let line = Line(point1: chart.refPointPosition(withKey: "AbortPoint")!, point2: chart.refPointPosition(withKey: "BADDN")!, color : .red, withGroundLine : true)
        chart.lines["MissedApproach"] = line

        chart.obstacles.append(Obstacle(position: (200,146), height: 416))
        chart.obstacles.append(Obstacle(position: (210,139), height: 470))
        chart.obstacles.append(Obstacle(position: (243,146), height: 596, name: "Washington Monument"))
        chart.obstacles.append(Obstacle(position: (263,144), height: 377, name: "Capitol"))
        chart.obstacles.append(Obstacle(position: (153,144), height: 866))
        chart.obstacles.append(Obstacle(position: (214,109), height: 18000, width: 20 * chart.mPerPt, name: "P-56B"))
        chart.obstacles.append(Obstacle(position: (214,245), height: 440))
        chart.obstacles.append(Obstacle(position: (282,250), height: 450))
        chart.obstacles.append(Obstacle(position: (290,195), height: 434))

        groundTracks["flightPath"] = flightPath(forChart: chart)
        groundTracks["arrival"] = arrivalPath(forChart: chart)
        groundTracks["missedApproachLoiter"] = missedApproachLoiter(forChart: chart)
        groundTracks["finalTurn"] = finalTurn(forChart: chart)

        return chart
    }()
    
    func flightPath(forChart chart : Chart) -> (UIBezierPath, Double, Double, Double?) {
        let path = UIBezierPath()
        path.move(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 0, y: 18.14)))
        path.addCurve(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 229, y: 5.14)),
                      controlPoint1: chart.deltaPtFromCenter(forPt: CGPoint(x: 89.25, y: -0.39)),
                      controlPoint2: chart.deltaPtFromCenter(forPt: CGPoint(x: 165.59, y: -4.72)))
        path.addCurve(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 382.85, y: 100.5)),
                      controlPoint1: chart.deltaPtFromCenter(forPt: CGPoint(x: 292.41, y: 15)),
                      controlPoint2: chart.deltaPtFromCenter(forPt: CGPoint(x: 329.32, y: 46.84)))
        path.addLine(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 402.31, y: 120.64)))
        path.addCurve(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 523.39, y: 247.29)),
                      controlPoint1: chart.deltaPtFromCenter(forPt: CGPoint(x: 478.81, y: 199.89)),
                      controlPoint2: chart.deltaPtFromCenter(forPt: CGPoint(x: 519.25, y: 242.78)))
        path.addCurve(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 533, y: 265.14)),
                      controlPoint1: chart.deltaPtFromCenter(forPt: CGPoint(x: 529.59, y: 254.06)),
                      controlPoint2: chart.deltaPtFromCenter(forPt: CGPoint(x: 532, y: 254.14)))
        path.addCurve(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 533.6, y: 278.14)),
                      controlPoint1: chart.deltaPtFromCenter(forPt: CGPoint(x: 533.33, y: 270.47)),
                      controlPoint2: chart.deltaPtFromCenter(forPt: CGPoint(x: 533.5, y: 274.81)))
        path.addLine(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 534.8, y: 288.0)))
        return (path, -293.0, -93.14, nil)
    }
    
    func arrivalPath(forChart chart : Chart) -> (UIBezierPath, Double, Double, Double?) {
        let path = UIBezierPath()
        path.move(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 402.0, y: 122.14)))
        path.addCurve(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 229.0, y: 5.14)),
           controlPoint1: chart.deltaPtFromCenter(forPt: CGPoint(x: 350.08, y: 54.0)),
           controlPoint2: chart.deltaPtFromCenter(forPt: CGPoint(x: 292.41, y: 15.0)))
        path.addCurve(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 0.0, y: 18.14)),
           controlPoint1: chart.deltaPtFromCenter(forPt: CGPoint(x: 165.59, y: -4.72)),
           controlPoint2: chart.deltaPtFromCenter(forPt: CGPoint(x: 89.25, y: -0.39)))
        return (path, -293.0, -93.14, nil)
    }
    
    func missedApproachLoiter(forChart chart : Chart) -> (UIBezierPath, Double, Double, Double?) {
        let rectangle = UIBezierPath()
        rectangle.move(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 16, y: 0)))
        rectangle.addLine(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 16, y: 0)))
        rectangle.addCurve(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 32, y: 16)),
                controlPoint1: chart.deltaPtFromCenter(forPt: CGPoint(x: 24.84, y: 0)),
                controlPoint2: chart.deltaPtFromCenter(forPt: CGPoint(x: 32, y: 7.16)))
        rectangle.addLine(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 32, y: 74)))
        rectangle.addCurve(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 16, y: 90)),
                controlPoint1: chart.deltaPtFromCenter(forPt: CGPoint(x: 32, y: 82.84)),
                controlPoint2: chart.deltaPtFromCenter(forPt: CGPoint(x: 24.84, y: 90)))
        rectangle.addLine(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 16, y: 90)))
        rectangle.addCurve(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 0, y: 74)),
                controlPoint1: chart.deltaPtFromCenter(forPt: CGPoint(x: 7.16, y: 90)),
                controlPoint2: chart.deltaPtFromCenter(forPt: CGPoint(x: 0, y: 82.84)))
        rectangle.addLine(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 0, y: 16)))
        rectangle.addCurve(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 16, y: 0)),
                controlPoint1: chart.deltaPtFromCenter(forPt: CGPoint(x: 0, y: 7.16)),
                controlPoint2: chart.deltaPtFromCenter(forPt: CGPoint(x: 7.16, y: 0)))
        rectangle.close()
//        context.translateBy(x: 238, y: 331)
//        context.rotate(by: 356 * CGFloat.pi/180)
//        context.translateBy(x: -16, y: -45)
        
        return (rectangle, 238 - 16, 331 - 45, 356°)
    }
    
    func finalTurn(forChart chart : Chart) -> (UIBezierPath, Double, Double, Double?) {
        let path2 = UIBezierPath()
        path2.move(to: chart.deltaPtFromCenter(forPt: CGPoint.zero))
        path2.addCurve(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 9, y: 17)),
            controlPoint1: chart.deltaPtFromCenter(forPt: CGPoint(x: 5.33, y: 4)),
            controlPoint2: chart.deltaPtFromCenter(forPt: CGPoint(x: 8.33, y: 9.67)))
        path2.addCurve(to: chart.deltaPtFromCenter(forPt: CGPoint(x: 9.5, y: 30)),
            controlPoint1: chart.deltaPtFromCenter(forPt: CGPoint(x: 9.33, y: 22.33)),
            controlPoint2: chart.deltaPtFromCenter(forPt: CGPoint(x: 9.5, y: 26.67)))
        return (path2, 231, 155, nil)
    }
}
