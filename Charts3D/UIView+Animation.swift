//
//  UIView+Animation.swift
//
//  Created by Duke Browning on 6/22/16.
//  Copyright (c) 2016 Atomic Skeeter Software. All rights reserved.
//

import UIKit

extension UIView {
    
    @objc func slideUp(numPixels : Int, duration : TimeInterval = 0.35) {
        UIView.animate(withDuration: duration) { [weak self] in
            if self == nil { return }
            self!.frame = CGRect(x: self!.frame.origin.x, y: self!.frame.origin.y - CGFloat(numPixels),
                                 width: self!.frame.size.width, height: self!.frame.size.height)
        }
    }
    
    @objc func slideDown(numPixels : Int, duration : TimeInterval = 0.35) {
        UIView.animate(withDuration: duration) { [weak self] in
            if self == nil { return }
            self!.frame = CGRect(x: self!.frame.origin.x, y: self!.frame.origin.y + CGFloat(numPixels),
                                 width: self!.frame.size.width, height: self!.frame.size.height)
        }
    }
    
    @objc func fadeIn( duration : TimeInterval = 0.5, completion: ((Bool)->Void)? = nil ) {
        if isHidden {
            alpha = 0.0
            isHidden = false
        }
        
        UIView.animate(withDuration: duration,
                                   animations : { [weak self] in
                                    self?.alpha = 1.0
            },
                                   completion : { finished in
                                    completion?(finished)
        })
    }
    
    @objc func fadeOut( duration : TimeInterval = 0.5, completion: ((Bool)->Void)? = nil ) {
        if isHidden { return }
        UIView.animate(withDuration: duration,
                                   animations : { [weak self] in
                                    self?.alpha = 0.0
            },
                                   completion : { finished in
                                    completion?(finished)
        })
    }
    
    @objc func animateConstraintChanges( duration : TimeInterval = 0.25, completion: ((Bool)->Void)? = nil ) {
        UIView.animate(withDuration: duration,
                                   animations : { [weak self] in
                                    self?.layoutIfNeeded()
            },
                                   completion : { finished in
                                    completion?(finished)
        })
    }
    
    @objc func removeAllAnimations(deeply : Bool = true) {
        layer.removeAllAnimations()
        
        if !deeply { return }
        
        for subview in subviews {
            subview.removeAllAnimations(deeply: deeply)
        }
    }
}
