//
//  OnOffView.swift
//  Terrain3D
//
//  Created by Duke Browning on 12/6/18.
//  Copyright © 2018 Atomic Skeeter Software, LLC. All rights reserved.
//

import Foundation
import UIKit

class OnOffView : UIView {
    
    static let height : CGFloat = 40
    static let outerSize : CGFloat = 30
    static let innerSize : CGFloat = 24
    static let gap : CGFloat = 5
    static var forKey = [String:OnOffView]()
    static var forRadioGroupKey = [String:[OnOffView]]()

    let leftJustified : Bool
    var handler : ((OnOffView,Bool)->Void)?
    var isOn : Bool {
        didSet {
            control.isHidden = !isOn
        }
    }
    var key : String
    var radioGroupKey : String?
    var controlWrapper = UIView.autolayout()
    var control = UIView.autolayout()
    let label = UILabel.autolayout()
    let text : String
    var color = UIColor.white {
        didSet {
            label.textColor = color
            controlWrapper.layer.borderColor = color.cgColor
            control.backgroundColor = color
        }
    }
    
    init(text : String, isOn : Bool = false,
                         key : String? = nil,
                  radioGroup : String? = nil,
               leftJustified : Bool = true,
               handler : ((OnOffView,Bool)->Void)?) {
        self.text = text
        self.isOn = isOn
        self.leftJustified = leftJustified
        self.key = key ?? text
        self.radioGroupKey = radioGroup
        self.handler = handler
        
        super.init(frame: CGRect.zero)
        
        translatesAutoresizingMaskIntoConstraints = false
        
        OnOffView.forKey[self.key] = self
        if let rgKey = radioGroupKey {
            var group = OnOffView.forRadioGroupKey[rgKey]
            if group == nil { group = [OnOffView]() }
            group?.append(self)
            OnOffView.forRadioGroupKey[rgKey] = group
        }

        addSubview(label)
        addSubview(controlWrapper)
        controlWrapper.addSubview(control)
        
        controlWrapper.layer.borderColor = color.cgColor
        controlWrapper.layer.borderWidth = 1.0
        controlWrapper.layer.cornerRadius = OnOffView.outerSize / 2.0
        controlWrapper.addViewAction { [weak self] in
            guard let this = self else { return }

            if let rKey = this.radioGroupKey, let toggles = OnOffView.forRadioGroupKey[rKey] {
                for t in toggles {
                    t.isOn = this.key == t.key
                }
            }
            else {
                this.isOn = !this.isOn
            }
            
            this.handler?(this,this.isOn)
        }
        
        control.layer.cornerRadius = OnOffView.innerSize / 2.0
        control.backgroundColor = color
        control.isHidden = !isOn
        
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = color
        label.text = text
        
        setNeedsUpdateConstraints()
    }
    
    var constraintsCreated = false
    func setupConstraints() {
        constraintsCreated = true

        constrainHeight(to: OnOffView.height)
        
        controlWrapper.constrainYAlignedToSuperview()
        controlWrapper.constrainWidth(to: OnOffView.outerSize)
        controlWrapper.constrainHeight(to: OnOffView.outerSize)
        
        control.centerInSuperview()
        control.constrainWidth(to: OnOffView.innerSize)
        control.constrainHeight(to: OnOffView.innerSize)
        
        label.constrainYAligned(to: control)
        
        if leftJustified {
            controlWrapper.constrainLeadingToSuperview()
            label.constrainAsTrailing(to: controlWrapper, withOffset: OnOffView.gap)
            label.constrainTrailingToSuperview()
        }
        else {
            controlWrapper.constrainTrailingToSuperview()
            label.constrainAsLeading(to: controlWrapper, withOffset: OnOffView.gap)
            label.constrainLeadingToSuperview()
        }
    }
    
    override func updateConstraints() {
        if !constraintsCreated { setupConstraints() }
        
        super.updateConstraints()
    }

    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
}

