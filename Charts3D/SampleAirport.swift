//
//  SampleAirport.swift
//
//  Created by Duke Browning on 11/25/18.
//  Copyright © 2018 Atomic Skeeter Software, LLC. All rights reserved.
//

import Foundation
import UIKit
import SceneKit

class SampleAirport : SCNNode {
    
    override init() {
        super.init()
        
        let runways = SCNShape(path: SampleAirport.runwaysPath(), extrusionDepth: 0.0)
        let material = SCNMaterial()
        material.diffuse.contents = UIColor.white
        material.locksAmbientWithDiffuse = true
        material.isDoubleSided = true
        runways.materials = [material]
        let runwaysNode = SCNNode(geometry: runways)
        addChildNode(runwaysNode)
    }
    
    static func runwaysPath() -> UIBezierPath {
        let runways = UIBezierPath()
        runways.move(to: CGPoint(x: 5.4, y: 12.14))
        runways.addLine(to: CGPoint(x: 9.1, y: 8.8))
        runways.addLine(to: CGPoint(x: 9.1, y: 0))
        runways.addLine(to: CGPoint(x: 11.1, y: 0))
        runways.addLine(to: CGPoint(x: 11.1, y: 7))
        runways.addLine(to: CGPoint(x: 14.57, y: 3.88))
        runways.addLine(to: CGPoint(x: 15.9, y: 5.37))
        runways.addLine(to: CGPoint(x: 11.1, y: 9.69))
        runways.addLine(to: CGPoint(x: 11.1, y: 21))
        runways.addLine(to: CGPoint(x: 9.1, y: 21))
        runways.addLine(to: CGPoint(x: 9.1, y: 11.5))
        runways.addLine(to: CGPoint(x: 5.4, y: 14.83))
        runways.addLine(to: CGPoint(x: 5.4, y: 18.8))
        runways.addLine(to: CGPoint(x: 3.4, y: 18.8))
        runways.addLine(to: CGPoint(x: 3.4, y: 16.63))
        runways.addLine(to: CGPoint(x: 1.34, y: 18.48))
        runways.addLine(to: CGPoint(x: 0, y: 17))
        runways.addLine(to: CGPoint(x: 3.4, y: 13.94))
        runways.addLine(to: CGPoint(x: 3.4, y: 4.1))
        runways.addLine(to: CGPoint(x: 5.4, y: 4.1))
        runways.addLine(to: CGPoint(x: 5.4, y: 12.14))
        runways.close()
        return runways
    }
    
    required init(coder: NSCoder) { fatalError("Not implemented.") }
}
