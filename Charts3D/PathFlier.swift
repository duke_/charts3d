//
//  PathFlier.swift
//
//  Created by Duke Browning on 11/29/18.
//  Copyright © 2018 Atomic Skeeter Software, LLC. All rights reserved.
//

import Foundation
import SceneKit

let animationDuration = 1.0

class LabeledPoint {
    
    init(text: String, position: SCNVector3) {
        self.text = text
        self.position = position
    }
    
    var text : String
    var position : SCNVector3
    var size : CGFloat = 400
    var color : UIColor = .green
}

class Line {
    
    var pt1 : SCNVector3
    var pt2 : SCNVector3
    var color : UIColor = .green
    var withGroundLine = false

    init(point1: SCNVector3, point2: SCNVector3, color : UIColor = .green, withGroundLine : Bool = false) {
        self.pt1 = point1
        self.pt2 = point2
        self.color = color
        self.withGroundLine = withGroundLine
    }
    
}

class ThreeDPath {
    
    static let defaultIntervalDuration = 0.5
    
    let altitudeProfile : AltitudeProfile
    let groundTrack : UIBezierPath
    var intervalDuration = ThreeDPath.defaultIntervalDuration
    let totalPts : Int

    init(groundTrack : UIBezierPath, altitudeProfile: AltitudeProfile, numPoints : Int = 40) {
        self.groundTrack = groundTrack
        self.altitudeProfile = altitudeProfile
        self.totalPts = numPoints
    }

    var path : [SCNVector3] {
        var pts = [SCNVector3]()
        let pts2D = groundTrack.points(numberOf: totalPts)
        guard pts2D.count > 0 else { return pts }
        let pctDelta = 1.0 / Double(pts2D.count)
        for (index, pt2D) in pts2D.enumerated() {
            pts.append(SCNVector3(pt2D.x, pt2D.y, CGFloat(altitudeProfile.altitude(atPercent: Double(index) * pctDelta))))
        }
        
        return pts
    }
    
    func actions(intervalDuration interval : Double? = nil) -> [SCNAction] {
        var actions = [SCNAction]()
        for pt in path {
            actions.append(SCNAction.move(to: pt, duration: interval ?? intervalDuration))
        }
        return actions
    }

}

class AltitudeProfile {
    
    // points are x : pct of total length, y = altitude
    var points = [CGPoint]()
    
    convenience init(distancePoints points : [CGPoint]) {

        var pts = [CGPoint]()
        if points.count < 2 {
            pts = points
        }
        else {
            let length = points.map { Double($0.x) }.reduce(0.0, + )
            
            var lastPt : CGPoint?
            var adjPt : CGPoint!
            for index in 0..<points.count {
                let pt = points[index]
                if lastPt != nil {
                    let dist = Double(lastPt!.x - pt.x)
                    adjPt = CGPoint(x: dist/length, y: Double(pt.y))
                }
                else {
                    adjPt = CGPoint(x: 0.0, y: pt.y)
                }
                
                pts.append(adjPt)
                lastPt = pt
            }
        }

        self.init(points: pts)
    }
    
    init(points : [CGPoint]) {
        if points.count == 0 {
            self.points = [CGPoint.zero, CGPoint(x: 1.0, y: 0.0)]
        }
        else if points.count == 1 {
            self.points = [CGPoint(x: 1.0, y: points[0].y), CGPoint(x: 1.0, y: points[0].y)]
        }
        else {
            self.points = points
        }
    }
    
    func altitude(atPercent x : Double) -> Double {
        guard points.count > 0 else { return 0.0 }

        guard x >= 0 && x <= 1.0 else {
            print("altitude(atPercent:) -- percent parameter should be between 0.0 and 1.0")
            return 0.0
        }

        var lastPt : CGPoint = points[0]
        var altitude = Double(lastPt.y)
        for index in 1..<points.count {
            let pt = points[index]
            let ptX = Double(pt.x)
            let ptY = Double(pt.y)
            if x > ptX {
                altitude = ptY
                lastPt = pt
                continue
            }

            let lastX = Double(lastPt.x)
            let lastY = Double(lastPt.y)
            let m = (lastY - ptY) / (lastX - ptX)
            altitude = m * (x - lastX) + lastY
        }
        
        return altitude
    }
    
}

class PathFlier {
    
    static func constructLandingPath(ontoRunway runwayEnd : AirportRunwayInfo.Runway.End,
                           usingApproachPath approachPath : UIBezierPath,
                                          altitudeProfile : AltitudeProfile) -> [SCNVector3] {
        
        let path = [SCNVector3]()
        
        return path
    }
}

public enum PathElement {
    
    case moveToPoint(CGPoint)
    case addLineToPoint(CGPoint)
    case addQuadCurveToPoint(CGPoint, CGPoint)
    case addCurveToPoint(CGPoint, CGPoint, CGPoint)
    case closeSubpath
    
    init(element: CGPathElement) {
        switch element.type {
        case .moveToPoint: self = .moveToPoint(element.points[0])
        case .addLineToPoint: self = .addLineToPoint(element.points[0])
        case .addQuadCurveToPoint: self = .addQuadCurveToPoint(element.points[0], element.points[1])
        case .addCurveToPoint: self = .addCurveToPoint(element.points[0], element.points[1], element.points[2])
        case .closeSubpath: self = .closeSubpath
        }
    }
}

public extension UIBezierPath {
    
    var elements: [PathElement] {
        var pathElements = [PathElement]()
        withUnsafeMutablePointer(to: &pathElements) { elementsPointer in
            cgPath.apply(info: elementsPointer) { (userInfo, nextElementPointer) in
                let nextElement = PathElement(element: nextElementPointer.pointee)
                let elementsPointer = userInfo!.assumingMemoryBound(to: [PathElement].self)
                elementsPointer.pointee.append(nextElement)
            }
        }
        return pathElements
    }
    
    func pointsOnCubicCurve(numSegments num : Int, start : CGPoint, c1: CGPoint, c2: CGPoint, end: CGPoint) -> [CGPoint] {
        var pts = [CGPoint]()
        guard num > 0 else { return pts }
        let incr = 1.0 / Double(num)
        var t = 0.0
        while t <= 1.0 {
            let t_: Double = (1.0 - t)
            let tt_: Double = t_ * t_
            let ttt_: Double = t_ * t_ * t_
            let tt: Double = t * t
            let ttt: Double = t * t * t
            let x = Double(start.x) * ttt_ + 3.0 * Double(c1.x) * tt_ * t + 3.0 * Double(c2.x) * t_ * tt + Double(end.x) * ttt
            let y = Double(start.y) * ttt_ + 3.0 * Double(c1.y) * tt_ * t + 3.0 * Double(c2.y) * t_ * tt + Double(end.y) * ttt
            pts.append(CGPoint(x: x, y: y))
            t += incr
        }
        return pts
    }
    
    func pointsOnQuadCurve(numSegments num : Int, start : CGPoint, c1: CGPoint, end: CGPoint) -> [CGPoint] {
        var pts = [CGPoint]()
        guard num > 0 else { return pts }
        let incr = 1.0 / Double(num)
        var t = 0.0
        while t <= 1.0 {
            let t_: Double = (1.0 - t)
            let tt_: Double = t_ * t_
            let tt: Double = t * t
            let x = Double(start.x) * tt_ + 2.0 * Double(c1.x) * t_ * t + Double(end.x) * tt
            let y = Double(start.y) * tt_ + 2.0 * Double(c1.y) * t_ * t + Double(end.y) * tt
            pts.append(CGPoint(x: x, y: y))
            t += incr
        }
        return pts
    }
    
    func pointsOnLine(pt1 : CGPoint, pt2 : CGPoint, numSegments num : Int = 1) -> [CGPoint] {

        var pts = [CGPoint]()
        
        guard num > 0 else { return pts }
        
        if num == 1 { return [pt1, pt2] }
        
        // y = mx + b
        // y2 - y1 = m(x2 - x1) ->
        let xDelta = (pt2.x - pt1.x) / CGFloat(num)
        
        if xDelta == 0 {
            let yDelta = (pt2.y - pt1.y) / CGFloat(num)
            for i in 0...num {
                let y = pt1.y + CGFloat(i) * yDelta
                pts.append(CGPoint(x: pt1.x, y: y))
            }
        }
        else {
            let m = (pt2.y - pt1.y) / (pt2.x - pt1.x)
            for i in 0...num {
                let x = CGFloat(i) * xDelta
                let y = m * x
                pts.append(CGPoint(x: x + pt1.x, y: y + pt1.y))
            }
        }
        
        return pts
    }
    
    func distanceBetween(pt1 : CGPoint, pt2 : CGPoint) -> Double {
        let xDelta = Double(pt2.x - pt1.x)
        let yDelta = Double(pt2.y - pt1.y)
        return (xDelta * xDelta + yDelta * yDelta).squareRoot()
    }

    func distanceBetween(points pts : [CGPoint]) -> Double {
        guard pts.count > 1 else { return 0.0 }
        var total = 0.0
        var index = 1
        var lastPt = pts[0]
        while index < pts.count {
            let pt = pts[index]
            total += distanceBetween(pt1: lastPt, pt2: pt)
            index += 1
            lastPt = pt
        }
        
        return total
    }
    
    func lengths(ofElements elements : [PathElement], withEstimationResolution segmentLength : Int = 10) -> [Double] {
        // compute total length
        var lastPt : CGPoint?
        var lengths = [Double]()
        
        for element in elements {
            switch element {
            case .moveToPoint(let pt):
                lengths.append(0.0)
                lastPt = pt
                
            case .addCurveToPoint(let pt, let c1, let c2):
                
                // estimate length with segments
                if let last = lastPt {
                    let dst = distanceBetween(pt1: last, pt2: pt)
                    if dst < 10 {
                        lengths.append(dst)
                    }
                    else {
                        let num : Int = Int(dst) / segmentLength
                        
                        if num < 2 {
                            lengths.append(dst)
                        }
                        else {
                            let pts = pointsOnCubicCurve(numSegments: num, start: last, c1: c1, c2: c2, end: pt)
                            lengths.append(distanceBetween(points: pts))
                        }
                    }
                }
                
                lastPt = pt
                
            case .addLineToPoint(let pt):
                if let last = lastPt {
                    lengths.append(distanceBetween(pt1: last, pt2: pt))
                }
                lastPt = pt
                
            case .addQuadCurveToPoint(let pt, let c1):
                
                // estimate length with segments
                if let last = lastPt {
                    let dst = distanceBetween(pt1: last, pt2: pt)
                    if dst < 10 {
                        lengths.append(dst)
                    }
                    else {
                        let num : Int = Int(dst) / segmentLength
                        
                        if num < 2 {
                            lengths.append(dst)
                        }
                        else {
                            let pts = pointsOnQuadCurve(numSegments: num, start: last, c1: c1, end: pt)
                            lengths.append(distanceBetween(points: pts))
                        }
                    }
                }
                
                lastPt = pt
                
            default:
                lengths.append(0.0)
            }
        }

        return lengths
    }
    
    func points(atInterval distance : Double) -> [CGPoint] {

        guard distance > 1 else { return [CGPoint]() }
        
        let elementLengths = lengths(ofElements: elements)
        let totalLength = elementLengths.reduce(0.0, +)
        
        let numPoints = Int((totalLength / distance).rounded())
        return points(numberOf: numPoints)
        
    }
    
    func points(numberOf num : Int) -> [CGPoint] {
        var pts = [CGPoint]()

        guard num > 1 else { return pts }
        
        let elementLengths = lengths(ofElements: elements)
        let totalLength = elementLengths.reduce(0.0, +)
        var lastPt : CGPoint?
        
        for (index,element) in elements.enumerated() {
            
            let numForPath = Int(((elementLengths[index] / totalLength) * Double(num)).rounded())

            switch element {
            case .moveToPoint(let pt1):
                pts.append(pt1)
                lastPt = pt1
                
            case .addCurveToPoint(let c1, let c2, let pt2):
                if let last = lastPt {
                    if last == pt2 { continue }
                    let points = pointsOnCubicCurve(numSegments: numForPath, start: last, c1: c1, c2: c2, end: pt2)
                    pts.append(contentsOf: points)
                }
                lastPt = pt2
                
            case .addLineToPoint(let pt3):
                if let last = lastPt {
                    if last == pt3 { continue }
                    let points = pointsOnLine(pt1: last, pt2: pt3, numSegments: numForPath)
                    pts.append(contentsOf: points)
                }
                lastPt = pt3
                
            case .addQuadCurveToPoint(let pt4, let c3):
                if let last = lastPt {
                    if last == pt4 { continue }
                    let points = pointsOnQuadCurve(numSegments: numForPath, start: last, c1: c3, end: pt4)
                    pts.append(contentsOf: points)
                }
                lastPt = pt4

            default:
                break
            }
        }
        return pts
    }
}

public extension SCNAction {
    
    class func moveAlong(path: UIBezierPath, altitude : CGFloat, withOffset offset : CGPoint? = nil) -> SCNAction {
        
        let points = path.elements
        var actions = [SCNAction]()
        
        let xOffset = offset?.x ?? 0
        let yOffset = offset?.y ?? 0

        for point in points {
            
            switch point {
            case .moveToPoint(let a):
                let moveAction = SCNAction.move(to: SCNVector3(a.x + xOffset, a.y + yOffset, altitude), duration: 0.0)
                actions.append(moveAction)
                break
                
            case .addCurveToPoint(let a, let b, let c):
                let moveAction1 = SCNAction.move(to: SCNVector3(a.x + xOffset, a.y + yOffset, altitude), duration: animationDuration)
                let moveAction2 = SCNAction.move(to: SCNVector3(b.x + xOffset, b.y + yOffset, altitude), duration: animationDuration)
                let moveAction3 = SCNAction.move(to: SCNVector3(c.x + xOffset, c.y + yOffset, altitude), duration: animationDuration)
                actions.append(moveAction1)
                actions.append(moveAction2)
                actions.append(moveAction3)
                break
                
            case .addLineToPoint(let a):
                let moveAction = SCNAction.move(to: SCNVector3(a.x + xOffset, a.y + yOffset, altitude), duration: animationDuration)
                actions.append(moveAction)
                break
                
            case .addQuadCurveToPoint(let a, let b):
                let moveAction1 = SCNAction.move(to: SCNVector3(a.x + xOffset, a.y + yOffset, altitude), duration: animationDuration)
                let moveAction2 = SCNAction.move(to: SCNVector3(b.x + xOffset, b.y + yOffset, altitude), duration: animationDuration)
                actions.append(moveAction1)
                actions.append(moveAction2)
                break
                
            default:
                let moveAction = SCNAction.move(to: SCNVector3(0, 0, altitude), duration: animationDuration)
                actions.append(moveAction)
                break
            }
        }
        return SCNAction.sequence(actions)
    }
}

//let path1 = UIBezierPath(roundedRect: CGRect(x: 1, y: 1, width: 2, height: 2), cornerRadius: 1)
//
//let moveAction = SCNAction.moveAlong(path: path1)
//let repeatAction = SCNAction.repeatForever(moveAction)
//SCNTransaction.begin()
//SCNTransaction.animationDuration = Double(path1.elements.count) * animationDuration
//boxNode.runAction(repeatAction)
//SCNTransaction.commit()
