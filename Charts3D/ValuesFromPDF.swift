//
//  ValuesFromPDF.swift
//  Terrain3D
//
//  Created by Duke Browning on 12/12/18.
//  Copyright © 2018 Atomic Skeeter Software, LLC. All rights reserved.
//

import Foundation

typealias Point = (Double,Double)
typealias TextPoints = [String:[Point]]

class ValuesFromPDF  {
    
    let pdfName : String
    var mask : CGRect?
    var offset : (Double,Double) = (0.0,0.0)
    
    init(named : String, offset : (Double,Double)? = nil, mask : CGRect? = nil) {
        self.pdfName = named
        self.mask = mask
        if offset != nil { self.offset = offset! }
    }
    
    var minX : Int {
        guard let rect = mask else { return 0 }
        return Int(rect.origin.x)
    }
    var maxX : Int {
        guard let rect = mask else { return 5000 }
        return minX + Int(rect.size.width)
    }
    var minY : Int {
        guard let rect = mask else { return 0 }
        return Int(rect.origin.y)
    }
    var maxY : Int {
        guard let rect = mask else { return 5000 }
        return minY + Int(rect.size.height)
    }

    func valueDict() -> TextPoints {

        var contents : String?
        if let documentPath = Bundle.main.path(forResource: pdfName , ofType: "txt", inDirectory: nil, forLocalization: nil) {
           let url = URL(fileURLWithPath: documentPath)
           contents = try? String(contentsOf: url, encoding: .utf8)
        }

        return valueDict(fromContents: contents ?? "")
    }

    func valueDict(fromContents contents : String) -> TextPoints {
        
        var textPoints = TextPoints() // : [String:[Point]]
        let scanner = Scanner(string: contents)
        while !scanner.isAtEnd {
            if !scanner.move(past: "(") { break }
            guard let element = scanner.scan(to: ")")?.trimmingCharacters(in: .whitespacesAndNewlines) else { continue }
            let subscanner = Scanner(string: element)
            var text = ""
            var found = false
            while let txt = subscanner.scan(to: ":") {
                if found { text += ":" }
                text += txt
                subscanner.move(past: ":")
                found = true
            }
            text = text.trimmingCharacters(in: .whitespacesAndNewlines)
            if text.isEmpty { continue }
            var x : Int = -1
            var y : Int = -1
            subscanner.scanInt(&x)
            subscanner.move(past: ",")
            subscanner.scanInt(&y)
            if x < minX || y > maxX || y < minY || y > maxY { continue }
            var points = textPoints[text] ?? [Point]()
            points.append((Double(x) + offset.0, Double(y) + offset.1))
            textPoints[text] = points
            if !scanner.move(past: ")") { break }
        }
        
        return textPoints
    }
}
