//
//  Obstacle.swift
//
//  Created by Duke Browning on 11/28/18.
//  Copyright © 2018 Atomic Skeeter Software, LLC. All rights reserved.
//

import Foundation

protocol ObstacleProvider {
    var obstacles : [Obstacle] { get }
}

class Obstacle {
    static let defaultDiameter = 100.0 // meters
    
    var name : String?
    var height : Double
    var width : Double = Obstacle.defaultDiameter
    var color : UIColor = .red
    var position : (Double, Double)
    
    init(position : (Double, Double), height : Double, width : Double = 100.0, color : UIColor = .red, name : String? = nil) {
        self.position = position
        self.height = height.ftToM
        self.width = width
        self.color = color
        self.name = name
    }

}
