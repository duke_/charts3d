//
//  DispatchTime.swift
//  Bookmobile
//
//  Created by Duke Browning on 3/22/16.
//  Copyright (c) 2016 Atomic Skeeter Software. All rights reserved.
//

import Foundation

// conveniences so that:
//    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DispatchTimeInterval.seconds(3)) { }
// can be written:
//    DispatchQueue.main.asyncAfter(deadline: 3.seconds.fromNow) { }

public extension Int {
    
    public var seconds : DispatchTimeInterval {
        return DispatchTimeInterval.seconds(self)
    }
    
    public var second : DispatchTimeInterval {  // for '1'
        return seconds
    }
    
    public var milliseconds : DispatchTimeInterval {
        return DispatchTimeInterval.milliseconds(self)
    }
    
    public var millisecond : DispatchTimeInterval { // for '1'
        return milliseconds
    }
    
}

public extension DispatchTimeInterval {
    public var fromNow : DispatchTime {
        return DispatchTime.now() + self
    }
}
