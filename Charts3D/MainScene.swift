//
//  MainScene.swift
//
//  Created by Duke Browning on 11/23/18.
//  Copyright © 2018 Atomic Skeeter Software, LLC. All rights reserved.
//

import UIKit
import SceneKit
import MapboxSceneKit

protocol ElevationProvider {
    func valueFor(x: Int, y: Int) -> Double?
}

class MainScene: SCNScene, SCNSceneRendererDelegate {

    static let useMapbox = true
    
    enum ViewMode {
        case original2D
        case decomposed
        case taxi
        case plate
        case approach
        case fly
    }
    
    var baseLayer : SCNNode!
    var airportChart : AirportDiagram!
    let approachChart = SceneKDCA.sngl.approachLDA_Z_Rwy_19

    var layers = [String:SCNNode]()
    static var layerVisibility = [String:Bool]()

    var taxiwayNodes = [String:[SCNNode]]()
    static var taxiwayVisibility = [String:Bool]()

    var backingNode : SCNNode?
    var mapboxTerrain : TerrainNode?
    var terrainGrid : TerrainGrid?
    var approachChartNode : SCNNode?
    var specifiedPathNode : SCNNode?
    var flightPathNode : SCNNode?
    var finalTurnNode : SCNNode?
    var missedApproachNode : SCNNode?
    var originalChartNode : SCNNode?
    var controlTowerNode : SCNNode?
    var centerPoint : SCNNode?
    var obstacleGroupNode : SCNNode?
    var labeledPtGroupNode : SCNNode?
    var labeledPtNodes = [String:SCNNode]()

    let viewMode : ViewMode
    
    init(viewMode : ViewMode) {
        self.viewMode = viewMode
        super.init()
        
        airportChart = SceneKDCA.sngl.airportDiagram
        baseLayer = SCNNode()
        rootNode.addChildNode(baseLayer)

        switch viewMode {
        case .decomposed:
            addChartLayers()
            for (key,node) in layers {
                node.isHidden = !(MainScene.layerVisibility[key] ?? true)
            }
        case .taxi:
            addRunways()
            addTaxiways()
            addTarmac()
            addBuildings()
        case .plate:
            addApproachChartNode()
        case .approach:
            addRunways()
            addTarmac()
            addBuildings()
            addControlTower()
            addObstacles(fromProvider: approachChart)
            addMapboxTerrain()
            addApproachChartNode()
            addSpecifiedPathNode()
            addFlightPathNode()
            flightPathNode?.isHidden = true
            addMissedApproach()
            addTerrain()
            addLabeledPoints()
            //addLines()
        case .fly:
            addRunways()
            addTarmac()
            // addBuildings()
            addObstacles(fromProvider: approachChart)
            addMapboxTerrain()
            addFlightPathNode()
            addMissedApproach()
            addTerrain()
            addLabeledPoints()
            //addLines()
        default:
            break
        }

    }
    
    func addRunways() {

        guard let runwayInfo = AirportRunwayInfo.forAirport[airportChart.airportCode] else { return }
        for runway in runwayInfo.runways {
            let geometry = SCNBox(width: CGFloat(runway.width.ftToM), height: CGFloat(runway.length.ftToM), length: 5.0, chamferRadius: 0.0)
            let material = SCNMaterial()
            let color = runway.isSelected ? UIColor.green : UIColor.red
            material.diffuse.contents = color
            geometry.materials = [material]
            let node = SCNNode(geometry: geometry)
            node.eulerAngles = SCNVector3(0.0, 0.0, -runway.end1.degreeMarker° + (AirportInfo.current?.magneticNorthDelta ?? 11)°)
            let offsetPt = airportChart.centerOffset(forRect: runway.bounds!)
            node.position = SCNVector3(offsetPt.x, -offsetPt.y, 0)
            rootNode.addChildNode(node)
            
            for end in [runway.end1, runway.end2] {
                let fontSize : CGFloat = viewMode == .approach || viewMode == .fly ? 64 : 64
                let geometry = SCNText(string: end.name, extrusionDepth: 0.0)
                geometry.font = UIFont.boldSystemFont(ofSize: fontSize)
                let material = SCNMaterial()
                material.diffuse.contents = color.withAlphaComponent(0.8)
                geometry.materials = [material]
                let node = SCNNode(geometry: geometry)
                if let chartPos = runway.chartPositions[end.name], let (x,y) = airportChart.deltasFromCenter(forChartXY: chartPos) {
                node.position = SCNVector3(x, y, 0)
                let constraint = SCNBillboardConstraint()
                constraint.freeAxes = .all
                node.constraints = [constraint]
                rootNode.addChildNode(node)
                }
            }

        }
    }
 
    func addTaxiways() {
        
        for taxiway in airportChart.taxiways {
            let taxiwaysNode = SCNNode()
            rootNode.addChildNode(taxiwaysNode)
            
            for pt in taxiway.1 {
                let fontSize : CGFloat = 36
                let geometry = SCNText(string: taxiway.0, extrusionDepth: 0.0)
                geometry.font = UIFont.systemFont(ofSize: fontSize)
                let material = SCNMaterial()
                material.diffuse.contents = UIColor.blue
                geometry.materials = [material]
                let node = SCNNode(geometry: geometry)
                if let (x,y) = airportChart.deltasFromCenter(forChartXY: pt) {
                    node.position = SCNVector3(x, y, 1)
                    let constraint = SCNBillboardConstraint()
                    constraint.freeAxes = .all
                    node.constraints = [constraint]
                    node.isHidden = true
                    taxiwaysNode.addChildNode(node)
                    var twayNodes = taxiwayNodes[taxiway.0] ?? [SCNNode]()
                    twayNodes.append(node)
                    taxiwayNodes[taxiway.0] = twayNodes
                }
            }
            
        }
    }
    
    func addTarmac() {
        
        let tarmacNode = SCNNode()
        rootNode.addChildNode(tarmacNode)

        var index = 0
        for path in KDCA_Tarmac.paths(forChart: airportChart) {
            let geometry = SCNShape(path: path, extrusionDepth: 4.0)
            let material = SCNMaterial()
            material.diffuse.contents = UIColor.gray
            geometry.materials = [material]
            let node = SCNNode(geometry: geometry)
            tarmacNode.addChildNode(node)
            let chartPos = KDCA_Tarmac.offsets[index]
            let (x,y) = airportChart.deltasFromCenter(forChartXY: chartPos)!
            node.position = SCNVector3(x, y, -4.0)
            index += 1
        }

        tarmacNode.position = SCNVector3(1173, -1500, 10)

//        if let offsetPt = airportChart.deltasFromCenter(forChartXY: (50,32)) {
//            tarmacNode.position = SCNVector3(offsetPt.0, -offsetPt.1, 10)
//        }
    }
    
    func addBuildings() {
        
        let buildingsNode = SCNNode()
        rootNode.addChildNode(buildingsNode)
        
        var index = 0
        for path in KDCA_Buildings.paths(forChart: airportChart) {
            let geometry = SCNShape(path: path, extrusionDepth: 30.0)
            let material = SCNMaterial()
            material.diffuse.contents = UIColor.purple
            geometry.materials = [material]
            let node = SCNNode(geometry: geometry)
            buildingsNode.addChildNode(node)
            let chartPos = KDCA_Buildings.offsets[index]
            let (x,y) = airportChart.deltasFromCenter(forChartXY: chartPos)!
            node.position = SCNVector3(x, y, -4.0)
            index += 1
        }
        
        buildingsNode.position = SCNVector3(1141, -1754, 25)
        
        //        if let offsetPt = airportChart.deltasFromCenter(forChartXY: (50,32)) {
        //            tarmacNode.position = SCNVector3(offsetPt.0, -offsetPt.1, 10)
        //        }
    }
    
   func updateVisibility(layerKey : String, to isOn : Bool) {
        MainScene.layerVisibility[layerKey] = isOn
        layers[layerKey]?.isHidden = !isOn
    }
    
    func updateTaxiwayVisibility(taxiway : String, to isOn : Bool) {
        MainScene.taxiwayVisibility[taxiway] = isOn
        if let nodes = taxiwayNodes[taxiway] {
            for node in nodes {
                node.isHidden = !isOn
            }
        }
    }
    
    var layerSpacing = 2.0

    func addChartLayers() {
//        if let keys = airportChart.runwayKeys, !keys.isEmpty {
//            let selectedKey : String? = airportChart.selectedRunwayKey
//            for key in keys {
//                if selectedKey != key {
//                    addLayer(withKey: key, isVisible: false, color: selectedKey == nil ? UIColor.green : UIColor.red)
//                }
//            }
//
//            if let key = selectedKey {
//                addLayer(withKey: key, isVisible: false, color: UIColor.green)
//            }
//        }

        for layer in airportChart.orderedLayers {
            addLayer(withKey: layer.key, isVisible: layer.isVisible, color: layer.color)
        }

    }
    
    func addApproachChartNode() {
        approachChartNode = planeNode(ewSpan: approachChart.mEW, nsSpan: approachChart.mNS, image: approachChart.contentImage)
        let (x,y) = airportChart.deltasFromCenter(forCoord: (airportChart.centerLat, airportChart.centerLng))
//        if let (vorPX, vorPY) = approachChart.refPts["VOR"] {
//            let (latP,lngP) = approachChart.coord(forPt: CGPoint(x: vorPX, y: vorPY))
//            (x,y) = airportChart.deltasFromCenter(forCoord: (latP,lngP))
//        }
        
        approachChartNode!.position = SCNVector3(x, y, -20.0)
        baseLayer.addChildNode(approachChartNode!)
    }
    
    func addFullApproachChartNode() {
        approachChartNode = planeNode(ewSpan: approachChart.mEW, nsSpan: approachChart.mNS, image: UIImage(named: "KDCA_LDA_Z_Rwy19_Full"))
        let (x,y) = airportChart.deltasFromCenter(forCoord: (airportChart.centerLat, airportChart.centerLng))
        approachChartNode!.position = SCNVector3(x, y, -20.0)
        baseLayer.addChildNode(approachChartNode!)
    }
    
    private func addLayer(withKey key : String, isVisible : Bool = true, color : UIColor? = nil) {

        var numImagesAdded = 0
        let layerNode = SCNNode()

        if numPlanesAdded == 0 {
            let node = planeNode(ewSpan: airportChart.mEW, nsSpan: airportChart.mNS, color: .white)
            numImagesAdded += 1
            node.position = SCNVector3(0, 0, Double(numPlanesAdded) * layerSpacing)
            layerNode.addChildNode(node)
        }

        for image in airportChart.images(forKey: key) {
            guard image != nil else { continue }
            var img = image
            if let color = color { img = img!.maskWithColor(color) }
            guard img != nil else { return }
            let node = planeNode(ewSpan: airportChart.mEW, nsSpan: airportChart.mNS, image: img!)
            numImagesAdded += 1
            node.position = SCNVector3(0, 0, Double(numPlanesAdded) * layerSpacing)
            layerNode.addChildNode(node)
        }

        if numImagesAdded > 0 {
            layers[key] = layerNode
            MainScene.layerVisibility[key] = isVisible
            layerNode.isHidden = !isVisible
            baseLayer.addChildNode(layerNode)
        }
    }
    
    private func replaceLayer(withKey key : String, isVisible : Bool = true, color : UIColor? = nil) {

        if let layer = layers[key] {
            layers[key] = nil
            layer.removeFromParentNode()
        }
        
        addLayer(withKey: key, isVisible: isVisible, color: color)
    }
    
    var numPlanesAdded = 0

    func planeNode(ewSpan: Double, nsSpan: Double, elev : Double = 0.0, image : UIImage? = nil, color: UIColor? = nil) -> SCNNode {
        numPlanesAdded += 1
        let geometry = SCNPlane(width: CGFloat(ewSpan), height: CGFloat(nsSpan))
        let material = SCNMaterial()
        material.diffuse.contents = image ?? (color ?? UIColor(white: 1.0, alpha: 0.5))
        geometry.materials = [material]
        return SCNNode(geometry: geometry)
    }
    
    func setLayer(withKey key : String, toVisible : Bool) {
        if let layer = layers[key] {
            layer.isHidden = !toVisible
        }
    }
    
    func addMapboxTerrain() {

                mapboxTerrain = TerrainNode(minLat:  40.0 + 30.0/60.0,  maxLat:    41.0 + 30.0/60.0,
                                            minLon: -(112 + 30.0/60.0), maxLon: -(111.0 + 50.0/60.0))

        // KSLC
//        mapboxTerrain = TerrainNode(minLat:  40.0 + 30.0/60.0,  maxLat:    41.0 + 30.0/60.0,
//                                    minLon: -(112 + 30.0/60.0), maxLon: -(111.0 + 50.0/60.0))

// KDCA
//        mapboxTerrain = TerrainNode(minLat: 38.0, maxLat: 39.0,
//                                    minLon: -77.5, maxLon: -76.2)

        mapboxTerrain!.fetchTerrainAndTexture(minWallHeight: 50.0, multiplier: 1.5, enableDynamicShadows: false,
              textureStyle: "mapbox/outdoors-v10",
            heightProgress: nil,
          heightCompletion: { [weak self] heightFetchError in
            guard let this = self else { return }
            if let heightFetchError = heightFetchError {
                NSLog("Texture load failed: \(heightFetchError.localizedDescription)")
            } else {
                NSLog("Terrain load complete")
                if this.terrainPending { this.addTerrain() }
            }
        },
          textureProgress: nil,
        textureCompletion: { image, textureFetchError in
            if let textureFetchError = textureFetchError {
                NSLog("Texture load failed: \(textureFetchError.localizedDescription)")
            }
            if image != nil {
                NSLog("Texture load for complete")
            }
        })
    }
    
    var terrainPending = false
    
    func addTerrain() {
    
        terrainPending = true
        
        var elevationProvider : (Int,Int)->Double?

        if MainScene.useMapbox {
            if mapboxTerrain == nil {
                addMapboxTerrain()
                return
            }
            elevationProvider = mapboxTerrain!.terrainHeight
        }
        else {
            NoiseGenerator.scale = 400.0
            let noiseGenerator = NoiseGenerator()
            elevationProvider = noiseGenerator.valueFor
        }

        let numRowsNS = 100.0
        let numColsEW = 100.0
        let terrainGridInterval = 1000.0
        
        terrainGrid = TerrainGrid(rowsNS: Int(numRowsNS), colsEW: Int(numColsEW), interval: terrainGridInterval) { [weak self] x, y in
            guard let this = self else { return nil }
            guard let value = elevationProvider(x, y) else { return nil }
            
            let baseElev = this.mapboxTerrain?.altitudeBounds.0 /* AirportInfo.current?.elevation */ ?? 0.0
            
            if value < baseElev { return nil }
            
            return (value - baseElev) * 1.5
        }
        
        terrainGrid!.position = SCNVector3(-numColsEW/2.0 * terrainGridInterval, -numRowsNS/2.0 * terrainGridInterval, 0)
        rootNode.addChildNode(terrainGrid!)
    }
    
    func pathNode(groundTrack : (UIBezierPath, Double, Double, Double?), altitudeProfile : AltitudeProfile, color : UIColor = .green) -> SCNNode {
        let path3D = ThreeDPath(groundTrack: groundTrack.0, altitudeProfile : altitudeProfile, numPoints: 80)

        let pts = path3D.path
        let node = SCNNode()
        var index = 0
        while index+1 < pts.count {
            let pt1 = pts[index]
            let pt2 = pts[index+1]
            var pt1_ = pt1
            pt1_.z = 0
            var pt2_ = pt2
            pt2_.z = 0
            node.addLine(pt1: pt1, pt2: pt2, color: color)
            node.addLine(pt1: pt1_, pt2: pt2_, color: color)
            node.addLine(pt1: pt1, pt2: pt1_, color: color)
            node.addLine(pt1: pt2, pt2: pt2_, color: color)
            index += 1
        }
        
        //            if let rotateBy = groundTrack.3 {
        //                node.localRotate(by: SCNQuaternion(0, 0, 1, rotateBy))
        //            }

        node.localTranslate(by: SCNVector3(Double(groundTrack.1) * approachChart.mPerPt,
                                           -Double(groundTrack.2) * approachChart.mPerPt, 0.0))
        return node
    }
    
    func addSpecifiedPathNode() {
        specifiedPathNode = SCNNode()
//        var altitude = AltitudeProfile(points: [CGPoint(x: 0, y: 3000.0.ftToM), CGPoint(x: 1.0, y: 9000.0.ftToM)])
//        specifiedPathNode!.addChildNode(pathNode(groundTrack: SceneKDCA.sngl.groundTracks["arrival"]!, altitudeProfile: altitude))
//        rootNode.addChildNode(specifiedPathNode!)
        
        let altitude = AltitudeProfile(points: [CGPoint(x: 0, y: 720.0.ftToM), CGPoint(x: 1.0, y: 10.0)])
        finalTurnNode = pathNode(groundTrack: SceneKDCA.sngl.groundTracks["finalTurn"]!, altitudeProfile: altitude)
        specifiedPathNode!.addChildNode(finalTurnNode!)
        
        specifiedPathNode?.addChildNode(lineNode(withKey: "FinalVector"))
        rootNode.addChildNode(specifiedPathNode!)
    }
    
   func addFlightPathNode() {
        let altitude = AltitudeProfile(points: [CGPoint(x: 0, y: 9000.0.ftToM), CGPoint(x: 0.9, y: 30.0), CGPoint(x: 1.0, y: 30.0)])
        flightPathNode = pathNode(groundTrack: SceneKDCA.sngl.groundTracks["flightPath"]!, altitudeProfile: altitude, color: .yellow)
        rootNode.addChildNode(flightPathNode!)
    }
    
    func addMissedApproach() {
        missedApproachNode = SCNNode()
        let altitude = AltitudeProfile(points: [CGPoint(x: 0, y: 3000.0.ftToM),CGPoint(x: 1.0, y: 3000.0.ftToM)])
        let node = pathNode(groundTrack: SceneKDCA.sngl.groundTracks["missedApproachLoiter"]!, altitudeProfile: altitude, color: .red)
        missedApproachNode!.addChildNode(node)
        missedApproachNode!.addChildNode(lineNode(withKey: "MissedApproach"))
        rootNode.addChildNode(missedApproachNode!)
    }
    
    func addCenterPoint() {
        centerPoint = addCylinder(x: 0, y: 0, height: 500, width: 10, color: UIColor.yellow)
        rootNode.addChildNode(centerPoint!)
    }

    func addControlTower() {
        controlTowerNode = addCylinder(x: -400, y: 450, height: 30, width: 30, color: UIColor.green)
        rootNode.addChildNode(controlTowerNode!)
    }

    func addObstacles(fromProvider provider : ObstacleProvider) {

        obstacleGroupNode = SCNNode()
        rootNode.addChildNode(obstacleGroupNode!)
        
        /*
        let (x,y) = airportChart.deltasFromCenter(forCoord: (airportChart.originLat, airportChart.originLng))
        addObstacle(x: x, y: y, height: 500, width: 10, color: UIColor.green)
        
        let (xR,yR) = airportChart.deltasFromCenter(forCoord: (airportChart.refLat, airportChart.refLng))
        addObstacle(x: xR, y: yR, height: 500, width: 10, color: UIColor.blue)
        
        let (xC,yC) = airportChart.deltasFromCenter(forCoord: (airportChart.centerLat, airportChart.centerLng))
        var node = addObstacle(x: xC, y: yC, height: 500, width: 150, color: UIColor.green)
        obstacleGroupNode!.addChildNode(node)

        let (xP,yP) = approachChart.deltasFromCenter(forCoord: (airportChart.centerLat, airportChart.centerLng))
        node = addObstacle(x: xP, y: yP, height: 500, width: 100, color: UIColor.yellow)
        obstacleGroupNode!.addChildNode(node)

        if let (x,y) = airportChart.deltasFromCenter(forRefKey: "VOR") {
            addObstacle(x: x, y: y, height: 500, width: 10, color: UIColor.red)
        }
        */
        
        for obstacle in provider.obstacles {
            guard let pt = approachChart.deltasFromCenter(forChartXY: (obstacle.position.0,obstacle.position.1)) else { continue }
            let node = addObstacle(x: pt.0, y: pt.1, height: obstacle.height, width: obstacle.width, color: obstacle.color)
            obstacleGroupNode!.addChildNode(node)
            
            if let name = obstacle.name {
                let geometry = SCNText(string: name, extrusionDepth: 0.0)
                geometry.font = UIFont.boldSystemFont(ofSize: 100)
                let material = SCNMaterial()
                material.diffuse.contents = obstacle.color
                geometry.materials = [material]
                let node = SCNNode(geometry: geometry)
                node.position = SCNVector3(pt.0 + obstacle.width/2.0 + 40, pt.1, 30)
                let constraint = SCNBillboardConstraint()
                constraint.freeAxes = .all //[.Z]
                node.constraints = [constraint]
                rootNode.addChildNode(node)
            }
        }

    }
    
    @discardableResult func addObstacle(x: Double, y: Double, height: Double = 100.0, width: Double? = nil, color : UIColor = .red, isTranslucent : Bool = true) -> SCNNode {
        return addCylinder(x: x, y: y, height: height, width: width ?? Obstacle.defaultDiameter, color: color, isTranslucent: isTranslucent)
    }
    
    func addCylinder(x: Double, y: Double, height: Double, width: Double, color : UIColor, isTranslucent : Bool = true) -> SCNNode {
        let geometry = SCNCylinder(radius: CGFloat(width/2.0), height: CGFloat(height))
        let materialSides = SCNMaterial()
        let hue = color
        if isTranslucent {
            materialSides.diffuse.contents = hue.withAlphaComponent(0.5)
            let materialTop = SCNMaterial()
            materialTop.diffuse.contents = hue.withAlphaComponent(0.9)
            let materialBottom = SCNMaterial()
            materialBottom.diffuse.contents = hue.withAlphaComponent(0.9)
            geometry.materials = [materialSides, materialTop, materialBottom]
        }
        else {
            let material = SCNMaterial()
            material.diffuse.contents = hue
            geometry.materials = [material]
        }
        let node = SCNNode(geometry: geometry)
        node.eulerAngles = SCNVector3(90°, 0, 0)
        node.position = SCNVector3(x, y, height/2.0 + 0.5)
        return node
    }
    
    func addLines() {
        
        for line in approachChart.lines.values {
            let node = SCNNode()
            node.addLine(pt1: line.pt1, pt2: line.pt2, color: line.color)
            rootNode.addChildNode(node)

            if line.withGroundLine {
                node.addLine(pt1: line.pt1.zeroZ, pt2: line.pt2.zeroZ, color: line.color)
                rootNode.addChildNode(node)
            }
        }
    }
    
    func lineNode(withKey key : String) -> SCNNode {
        
        let node = SCNNode()
        guard let line = approachChart.lines[key] else { return node }
        node.addLine(pt1: line.pt1, pt2: line.pt2, color: line.color)
        
        if line.withGroundLine {
            node.addLine(pt1: line.pt1.zeroZ, pt2: line.pt2.zeroZ, color: line.color)
        }
        
        return node
    }
    
   func addLabeledPoints(withVerticalLineToGround withLine : Bool = true) {

        labeledPtGroupNode = SCNNode()
        rootNode.addChildNode(labeledPtGroupNode!)

        for labeledPt in approachChart.labeledPoints.values {
            let lptNode = addLabeledPoint(labeledPt)
            labeledPtNodes[labeledPt.text] = lptNode
            if withLine {
                let pt = labeledPt.position
                var pt_ = pt
                pt_.z = 0
                labeledPtGroupNode!.addLine(pt1: pt, pt2: pt_, color: labeledPt.color)
            }
            labeledPtGroupNode!.addChildNode(lptNode)
        }
    }
    
    func addLabeledPoint(_ lpt : LabeledPoint) -> SCNNode {
        let geometry = SCNText(string: lpt.text, extrusionDepth: 0.0)
        geometry.font = UIFont.boldSystemFont(ofSize: lpt.size)
        let material = SCNMaterial()
        material.diffuse.contents = lpt.color
        geometry.materials = [material]
        let node = SCNNode(geometry: geometry)
        // node.eulerAngles = SCNVector3(90°, 0, 0)
        node.position = lpt.position
        let constraint = SCNBillboardConstraint()
        constraint.freeAxes = .all
        node.constraints = [constraint]
        return node
    }
    

    func addOmniLight() {
        let lightNode = SCNNode()
        lightNode.light = SCNLight()
        lightNode.light!.type = SCNLight.LightType.omni
        lightNode.light!.color = UIColor.white
        lightNode.position = SCNVector3(x: 0, y: 0, z: 100)
        rootNode.addChildNode(lightNode)
    }
    
    required init(coder: NSCoder) { fatalError("Not implemented.") }
}
