//
//  UIView+Autolayout.m
//  Bookmobile
//
//  Created by Duke Browning on 7/30/17.
//  Copyright © 2017 Atomic Skeeter Software. All rights reserved.
//

#import "UIView+Autolayout.h"

@implementation UIView (Autolayout)

+(instancetype) autolayout {
    UIView *view = [self new];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    return view;
}

-(instancetype) autolayout {
    self.translatesAutoresizingMaskIntoConstraints = NO;
    return self;
}

@end
