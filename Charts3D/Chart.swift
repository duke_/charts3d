//
//  Chart.swift
//  Terrain3D
//
//  Created by Duke Browning on 12/3/18.
//  Copyright © 2018 Atomic Skeeter Software, LLC. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit
import SceneKit

// Each chart has a reference point (measured from top/left origin) with a descernible lat/long.

class Chart : ObstacleProvider {
    
    class Layer {
        let key : String
        var isVisible : Bool
        var color : UIColor?
        
        init(key : String, isVisible : Bool = true, color : UIColor? = nil) {
            self.key = key
            self.isVisible = isVisible
            self.color = color
        }
    }
    
    let ftPerMinuteLat = 6068.0
    let mPerMinuteLat = 6068.0.ftToM // Double { return ftPerMinuteLat.ftToM }
    var mPerMinuteLng : Double { return ptPerMinuteLng / ptPerMinuteLat * mPerMinuteLat }

    let refImageName : String

    // provided values (ML scan of contentImage)
    let airportCode : String
    let refPt : CGPoint
    let refLat : Double
    let refLng : Double
    let ptPerMinuteLat : Double
    let ptPerMinuteLng : Double

    // convenience conversions
    var minutesEW : Double { return Double(contentImage.size.width) / ptPerMinuteLng }
    var minutesNS : Double { return Double(contentImage.size.height) / ptPerMinuteLat }
    var refPtX : Double { return Double(refPt.x) }
    var refPtY : Double { return Double(refPt.y) }
    var ptPerDegreeLat : Double { return ptPerMinuteLat * 60.0 }
    var ptPerDegreeLng : Double { return ptPerMinuteLng * 60.0 }

    var centerLat : Double {
        let pt = CGPoint(x: contentImage.size.width / 2.0, y: contentImage.size.height / 2.0)
        let coords = coord(forPt: pt)
        // print("center: \(coordDebug(coord: coords))")
        return coords.0
    }
    var centerLng : Double {
        let pt = CGPoint(x: contentImage.size.width / 2.0, y: contentImage.size.height / 2.0)
        let coords = coord(forPt: pt)
        return coords.1
    }
    
    let originX = 0.0
    let originY = 0.0
    var originLat : Double {
        let lat = refLat + ((refPtY - originY) / ptPerMinuteLat) / 60.0
        // print("origin lat: \(coordDebug(coord: lat))")
        return lat
    }
    var originLng : Double {
        let lng = refLng - ((refPtX - originX) / ptPerMinuteLng) / 60.0
        // print("origin lng: \(coordDebug(coord: lng))")
        return lng
    }

    var contentImage : UIImage { return UIImage(named: refImageName)! }
    
    var layers = [String:Layer]()
    var orderedLayerKeys = [String]()
    var images = [String:UIImage]()
    var imageKeysByLayerKey = [String:[String]]() // [layerKey:[imageKey]]
    
    var refCoords = [String:(Double,Double)]()
    var refPts = [String:(Double,Double)]()
    var refAlts = [String:Double]()
    var labeledPoints = [String:LabeledPoint]()
    var lines = [String:Line]()
    var obstacles : [Obstacle] = [Obstacle]()
    var taxiways = TextPoints()
    
    // distance is constant across latitudes, so use it to determine scale for both directions
    var _mEW : Double?
    var _mNS : Double?
    var _mPerPt : Double?
    var mEW : Double {
        if _mEW == nil { _mEW = minutesEW * mPerMinuteLng }
        return _mEW!
    }
    var mNS : Double {
        if _mNS == nil { _mNS = minutesNS * mPerMinuteLat }
        return _mNS!
    }
    var mPerPt : Double {
        if _mPerPt == nil { _mPerPt = abs(mPerMinuteLat / ptPerMinuteLat) }
        return _mPerPt!
    }
    
    var imageNameProvider : ((String)->String)? // image name from image key algorithm, such as prepending airport code ("KDCA_")
    
    init(code : String, refImageName: String,
       refPt  : CGPoint, refLat : Double, refLng : Double,
       ptPerMinuteLat : Double, ptPerMinuteLng : Double) {
        self.airportCode = code
        self.refImageName = refImageName
        self.refPt = refPt
        self.refLat = refLat
        self.refLng = refLng
        self.ptPerMinuteLat = ptPerMinuteLat
        self.ptPerMinuteLng = ptPerMinuteLng
        // print(refImageName + " ref: \(coordDebug(coord: (refLat,refLng)))")
    }
    
    func addLayer(withKey key : String, isVisible : Bool = true, color : UIColor? = nil) {
        let layer = Layer(key: key, isVisible: isVisible, color: color)
        orderedLayerKeys.append(layer.key)
        layers[key] = layer
    }
    
    var orderedLayers : [Layer] { return orderedLayerKeys.compactMap { layers[$0] }}
    
    // UIImages are not created until they're asked for
    func image(forKey imageKey: String) -> UIImage? {

        let key = imageNameProvider?(imageKey) ?? imageKey
        
        if let image = images[key] {
            return image
        }
        else if let image = UIImage(named: key) {
            images[key] = image
            return image
        }
        
        return nil
    }
    
    // if a layer for the key is found, its imageKeys are returned
    // if an image for the key is found, the key is wrapped in an array and returned
    func imageKeys(forKey key : String) -> [String] {
        if let layerKeys = imageKeysByLayerKey[key] { return layerKeys }
        if image(forKey: key) != nil { return [key] }
        return [String]()
    }
    
    func images(forKey key : String) -> [UIImage?] {
        var imgs = [UIImage?]()
        for imageKey in imageKeys(forKey: key) {
            imgs.append(image(forKey: imageKey))
        }
        return imgs
    }
    
    func coord(forPt pt : CGPoint) -> (Double, Double) {
        let lng = originLng + Double(pt.x) / ptPerDegreeLng
        let lat = originLat - Double(pt.y) / ptPerDegreeLat
        return (lat, lng)
    }
    
    func coord(forChartXY pt : (Double,Double)) -> (Double, Double) {
        let lng = originLng + Double(pt.0) / ptPerDegreeLng
        let lat = originLat - Double(pt.1) / ptPerDegreeLat
        return (lat, lng)
    }
    
    func coord(forKey key : String) -> (Double,Double)? {
        if let coords = refCoords[key] { return coords }
        guard let (x,y) = refPts[key] else { return nil }
        return coord(forPt: CGPoint(x: x, y: y))
    }
    
    func deltasFromCenter(forCoord latLng : (Double, Double)) -> (Double,Double) {
        let x = (latLng.1 - centerLng) * ptPerDegreeLng
        let y = (latLng.0 - centerLat) * ptPerDegreeLat
        return (x * mPerPt, y * mPerPt)
    }
    
    func deltasFromCenter(forRefKey key : String) -> (Double,Double)? {
        guard let coords = coord(forKey: key) else { return nil }
        return deltasFromCenter(forCoord: coords)
    }
    
    func deltasFromCenter(forChartXY pt : (Double,Double)) -> (Double,Double)? {
        return deltasFromCenter(forCoord: coord(forChartXY: pt))
    }
    
    func deltaPtFromCenter(forPt pt : CGPoint) -> (CGPoint) {
        let (x,y) = deltasFromCenter(forCoord: coord(forPt: pt))
        return CGPoint(x: x, y: y)
    }
    
    func centerOffset(forRect rect : CGRect) -> CGPoint {
        let x = (rect.origin.x + rect.size.width * 0.5) * CGFloat(mPerPt) - CGFloat(mEW) * 0.5
        let y = (rect.origin.y + rect.size.height * 0.5) * CGFloat(mPerPt) - CGFloat(mNS) * 0.5
        return CGPoint(x: x, y: y)
    }
    
    func refPointPosition(withKey key : String) -> SCNVector3? {
        if let (x,y) = deltasFromCenter(forRefKey: key), let z = refAlts[key] {
            return SCNVector3(x, y, z)
        }
        
        return nil
    }
    
    func coordDebug(coord : Double) -> String {
        let fmtrDeg = NumberFormatter()
        fmtrDeg.numberStyle = .decimal
        fmtrDeg.maximumFractionDigits = 3
        let fmtrMin = NumberFormatter()
        fmtrMin.numberStyle = .decimal
        fmtrMin.maximumFractionDigits = 2
        let intCoord = Int(coord)
        return "\(fmtrDeg.string(for: coord) ?? "")° (\(intCoord)° \(fmtrMin.string(for: abs(coord - Double(intCoord)) * 60.0) ?? "")')"
    }

    func coordDebug(coord : (Double, Double)) -> String {
        return "lat: " + coordDebug(coord: coord.0) + " lng: " + coordDebug(coord: coord.1)
    }
}

