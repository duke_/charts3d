//
//  AirportInfo.swift
//
//  Created by Duke Browning on 11/27/18.
//  Copyright © 2018 Atomic Skeeter Software, LLC. All rights reserved.
//

import Foundation
import UIKit

protocol AirportInfoProvider {
    var airportCode : String { get }
    var latitude : Double { get }
    var longitude : Double { get }
    var elevation : Double { get }
}

class AirportInfo {
 
    static var selectedAirportCode : String?
    static var forAirportCode = [String:AirportInfo]()
    static var current : AirportInfo? { return forAirportCode[selectedAirportCode ?? "none"] }

    init(airportCode : String, latitude : Double, longitude : Double, elevation : Double) {
        self.airportCode = airportCode
        self.latitude = latitude
        self.longitude = longitude
        self.elevation = elevation
        AirportInfo.forAirportCode[airportCode] = self
    }
    
    init(provider : AirportInfoProvider) {
        airportCode = provider.airportCode
        latitude = provider.latitude
        longitude = provider.longitude
        elevation = provider.elevation
        AirportInfo.forAirportCode[airportCode] = self
    }
    
    let airportCode : String
    let latitude : Double
    let longitude : Double
    let elevation : Double
    var magneticNorthDelta = 11.0

    static func convertLatitude(degrees: Double, minutes: Double, inNorthernHemisphere : Bool = true) -> Double {
        return (degrees + minutes/60.0) * (inNorthernHemisphere ? 1.0 : -1.0)
    }
    
    static func convertLongitude(degrees: Double, minutes: Double, inWesternHemisphere : Bool = true) -> Double {
        return (degrees + minutes/60.0) * (inWesternHemisphere ? -1.0 : 1.0)
    }
}
