3D Charts XCode project from Winter Gathering 2018.
Uses Swift, SceneKit and Mapbox.

Steps to build:
carthage bootstrap
Build MapboxScenedKit target.
Build Charts3D target.

Using the app:
Radio buttons across the top step through the demo.
Vertical radio buttons on different screens allow filtering.
3D screens are interactive pinch, pan, etc..
